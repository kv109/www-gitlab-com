---
layout: markdown_page
title: "Group Direction - License"
description: This is the direction page for the Provision group which is part of the Fulfillment stage. Learn more here!
canonical_path: "/direction/fulfillment/license"
---

- TOC
{:toc}


### Introduction and how you can help
Thanks for visiting the direction page for the License group within the Fulfillment Section in GitLab. This page is being actively maintained by [Teresa Tison](https://gitlab.com/teresatison). This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute.



### Overview
The License group within the Fulfillment Section takes care of all of the post-purchase/trial activities which occur behind the scenes at GitLab. This includes Licenseing purchased services via [LicenseDot](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#licensedot), and delivering information about purchases and trials to critical business systems like Salesforce, [Zuora](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#zuora) and Marketo. The License group is also responsible for the overall Fulfillment system architecture and data integrity.

### Engineering

The Fulfillment sub-department handbook page has a [license section](/handbook/engineering/development/fulfillment/#license)


#### Challenges to address
We have technical debt and system architecture constraints to work through, with a keen awareness of the need to unwind data redundancy throughout our ecosystem. As we work to make [Zuora our SSOT](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/213) for all subscription data, we will explore the [database architecture](https://gitlab.com/groups/gitlab-org/-/epics/4421) of [CustomersDot](https://about.gitlab.com/handbook/engineering/development/fulfillment/architecture/#customersdot) and the [future of customers.gitlab.com](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/101). 

Another challenge which is top of mind, is the current limitations of our provisioning processes. On the self-managed side, we have customer and internal stakeholder (sales, support, finance) inefficiencies in manually ensuring customers get off to a productive start post purchase. We are looking to isolate all of the reasonas this occurs (many of which are documented on our Q4 OKRs) and solve for driving as much business as possible through self-service transactions without manual follow-up after purchase. _Note, we're [creating a company KPI](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/65968) to measure our success in this area._




#### What's Next & Why
_coming soon_


#### What is Not Planned Right Now
While we're thinking about making [Zuora our SSOT](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/213), this is not a Q4 deliverable at this time. 



