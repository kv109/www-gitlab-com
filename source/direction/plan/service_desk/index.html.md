---
layout: markdown_page
title: "Category Direction - Service Desk"
description: View the GitLab category strategy for Service Desk, part of the Plan stage's Certify group. Find more information here!
canonical_path: "/direction/plan/service_desk/"
---

- TOC
{:toc}

## Service Desk

|                       |                               |
| -                     | -                             |
| Stage                 | [Plan](/direction/plan/)      |
| Maturity              | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-10-20`                  |

### Overview

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

#### Purpose

Great products need to offer a great support experience. The GitLab Service Desk aims to be the primary medium which connects customers to product support staff.

Service desk allows your organization the opportunity to provide an email address to your customers. These customers can send issues, feature requests, comments, and suggestions via email, with no external tools needed. These emails become issues right inside GitLab, potentially even in the same project where you are developing your product or service, pulling your customers directly into your DevOps process.


#### Intent

In an effort to clearly define a concrete and inspirational intent, it is important to answer this single question -- *"If Service Desk can be truly excellent at only one thing, what would it be?"* This is the intent of the Service Desk:

> To provide a conduit through which customers and support staff can **effectively collaborate** using **familiar process flows** to achieve  **prompt problem resolution**.

At GitLab, we don't really have a concept of `done`, but instead believe we should continue to iterate toward a more mature product as defined by our [maturity framework](https://about.gitlab.com/direction/maturity/). To better clarify our strategy, we must first understand what it will mean to have achieved Lovable Maturity. This is how we will know:

It is important to clearly define the desired user experience for a feature like the Service Desk. Not only do we desire to make providing support and issue resolution fluid and collaborative, but this feature can be exposed to the end-user. Ensuring that end-users receive exceptionally high quality communication is imperative to both us and our customers.

**Intuitive:** When users find issue or need support, communication should be as simple as possible -- ideally utilizing existing collaboration mechanisms. Currently, we support email integration in an effort to make requests as simple as sending an email. We're also exploring ways to integrate other communication channels such as Slack, to provide additional ways of reaching out to support teams. Since the Service Desk creates issues, the entire host of issue tracking and management tools can be utilized.

**Collaborative:** Sometimes it takes a team to resolve an end-user's problem. We're attempting to break down the silo surrounding help-desk requests and bring those issues into the existing issue tracking paradigm. Support Engineers can easily tag software developers, security analysts, or any other team members who can all share a single issue and therefore a single source of truth.

**Efficient:** Support is requested when features are missing or problems arise. This can be a stressful time, so it's important to ensure fast and accurate support interactions. Notes, comments, and any other internal / customer interactions should be easily available for all necessary parties. We are also looking for ways to provide support metrics, to track time to resolution as well as repeat issues.

**Intelligent:** We are looking to leverage [autonomation](https://en.wikipedia.org/wiki/Autonomation) to decrease human intervention and improve the accuracy of support interactions. We want to strive for a few manual steps as possible for categorization, triage, and other administrative tasks to allow the support team to spend more time providing valuable customer interactions and resolving issues. Moreover, we want to automate the routing of support tickets to the right team when they are created. 


#### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

Earlier this year, we made the decision to [move Service Desk to our Core product](https://gitlab.com/groups/gitlab-org/-/epics/3103). This has been completed, allowing users of all tiers to enjoy the benefits of Service Desk.

One of our one year goals is to improve communication flow between the end-user and the support team. To accomplish this, we are undertaking the following:

- Identify and resolve issues with text formatting and attachment handling within the service desk. It should be a seamless process to engage in bi-directional communication between an end-user and the service desk to collaborate using formatted text and attachments.
- It should be easy to add additional end-users and support team members to an issue, and they should receive the expected level of communication surrounding that issue.

Another goal is to make the Service Desk an integral part of the GitLab support workflow.

- We strive to [dogfood everything](/handbook/values/#dogfooding). We are in the process of interviewing and collaborating with our internal customers to make the Service Desk a much more widely utilized feature. Our hope is that through our own use, we will identify which additional features would allow the service desk to be a truly collaborative and efficient environment.

We also intend to make use of [on-call schedule management](https://gitlab.com/groups/gitlab-org/-/epics/3960) which is being built by the [Monitor:Health group](https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/). Once completed, that feature set will allow managers to put support teams on-call and to automate the routing of support tickets to the right team member.

### Target Audience

The target audience for the Service Desk is as follows:

- Support Engineer - These engineers are the customer facing representatives of the business, and want to be able to efficiently resolve problems as they arise. They are frustrated by manual steps which divert their focus from solving real problems for the customers they serve, and strive to represent their company in the best way possible.
- Software Developers - Software developers may be called on by the Support Engineers to collaborate on customer issues. They want to be able to easily understand the issue, and provide timely feedback. They are frustrated by needing to work in a separate system as it delays collaboration and creates a disjointed workflow.

### What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

Over the next few releases, we will be focusing on the following issues & epics:

- [Allow private comments on all commentable resources](https://gitlab.com/groups/gitlab-org/-/epics/2697) - (👍 43)
  - **Outcome:** Allow users to mark comments as private, allowing only project members to view the comment
- [Customize Service Desk to reflect our business, not GitLab specifically](https://gitlab.com/groups/gitlab-org/-/epics/1991) - (👍 28)
  - **Outcome:** Allow for a more seamless experience between the support team and the customer
- [Service Desk functionality insufficient for IT Helpdesk use](https://gitlab.com/groups/gitlab-org/-/epics/2383) - (👍 15)
  - **Outcome:** Improve communication between customers and support team

The Certify group level [issue board](https://gitlab.com/groups/gitlab-org/-/boards/1235846?&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Acertify) provides insight into everything currently in flight.


### What is Not Planned in Next Six Months
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

Given the amount of amazing ideas we receive, it's not always possible to implement everything in the near future. This section shows ideas that we haven't forgotten about, but simply cannot schedule in the near term.

- Creating a web interface for the Service Desk. While this may be important in the future, right now it is more important to focus on improving the existing experience for the customer and support team. We want to ensure we have a solid foundation for issue report, issue triage and issue resolution.


### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the **viable** level, and our next maturity target is **complete** by 2021-04-30.

We are tracking progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1704).

We are also tracking our progress toward **lovable** via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1705).

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
We are currently using the [loose Stage Monthly Active Users (SMAU) definition](/handbook/product/categories/plan/#metrics) and intend on migrating to the strict definition as soon as we've implemented the necessary telemetry to measure the defined events.

We are also hoping to collect [AMAU](https://about.gitlab.com/handbook/product/metrics/#actions-monthly-active-users-amau) statistics for the following actions once the necessary product analytics data is available.

- Service Desk issue created
- Service Desk issue collaboration (comments)
- Service Desk issue resolved


### Why is this important?
<!--
- Why is GitLab building this feature?
- Why impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
Traditionally, product managers and developers work out of separate tools from the support staff. This severely limits the amount of customer feedback visible to the product managers and developers and leaves the support staff struggling to effectively collaborate with technical and product resources. By bringing customer support into the DevOps Platform, we are enabling customer feedback to flow directly into bugs, feature requests, and regression reports. We are also making product and technical support easily available to support staff, decreasing time to resolution for support issues!


### Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Zendesk and Freshdesk are popular tools to do customer support management, and we can learn from these tools used by many organizations. Jira Service Desk leverages the existing Jira issue tracker to get customer tickets into Jira.

GitLab Service Desk takes inspiration from Jira Service Desk to also get customer tickets into GitLab. In GitLab, the benefits are even more pronounced since a single customer ticket is turned into a GitLab issue which can be incorporated into both broader portfolio management features of GitLab, and also downstream with product-development team sprints.

### Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We are working to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

<!--### Top Customer Success/Sales issue(s)-->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

| Issue                                                                                                     | 👍 |
| -                                                                                                         | -  |
| [Service Desk should add CC'ed users to the issue](https://gitlab.com/gitlab-org/gitlab/issues/4652)       | 17 |
| [Configuratble email address for service desk](https://gitlab.com/gitlab-org/gitlab/-/issues/2201)        | 15 |
| [Asdd issue participants via email address](https://gitlab.com/gitlab-org/gitlab/-/issues/195525)         | 13 |
| [Customer Database for Service Desk](https://gitlab.com/gitlab-org/gitlab/issues/2256)                    | 12 |


### Top dogfooding issues
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

| Issue                                                                                                             |
| -                                                                                                                 |
| [Slack Integration for Service Desk](https://gitlab.com/gitlab-org/gitlab/issues/195515)                          |
| [Imporove Markdown support within Service Desk](https://gitlab.com/gitlab-org/gitlab/issues/195366)               |
| [Initiate an issue from Service Desk to include the customer](https://gitlab.com/gitlab-org/gitlab/issues/195525) |
| [Allow threaded responses for Service Desk](https://gitlab.com/gitlab-org/gitlab/issues/195372)                   |
