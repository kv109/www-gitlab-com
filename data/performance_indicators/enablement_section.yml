- name: Enablement - Section PPI, Stage PPI - Median End User Page Load Time
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user page performance collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - We seem to have had a performance regression, which is showing up in our page load times. This is corroborated by our Sitespeed testing, with 13% fewer pages <2.5s and average LCP growing by 0.5s. We [need to investigate](https://gitlab.com/gitlab-org/gitlab/-/issues/326154). This does not seem related to our stability challenges recently.
  implementation:
    status: Target
    reasons:
      - We are still working to [improve our per-geo views](https://gitlab.com/gitlab-data/analytics/-/issues/8014).
      - Need to identify a target.
  lessons:
    learned:
    - We have made improvements to LCP, however [TTI/TBT are still significant gaps](https://forgeperf.org/) vs. our primary competition
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657
  metric_name: performanceTiming
  sisense_data:
    chart: 10546836
    dashboard: 794513
    embed: v2
  sisense_data_secondary:
    chart: 10603331
    dashboard: 794513
    embed: v2

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - Continuing to fluctate in the high 20's, the overall trend of increasing age continues.
    - In order to achieve our target, we will need to reverse the broader trend.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - Overall the trend of the age of instances from 2017 [still continues to climb](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380), however it appears that the trend is is beginning to slow. This will need to be monitored next month to see if this remains true or if the trend reverts back to a higher rate.
    - The [Helm chart](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8890933&udv=1059380) is still well above target, and when controlling for official Distribution artifacts the metric improves into the 30's.
    - We are still in the process of analyzing [the initial feedback](https://gitlab.com/gitlab-org/ux-research/-/issues/1301) now. We have also recieved some results from this [study](https://gitlab.com/gitlab-org/ux-research/-/issues/1250#note_526369784), which was focused on the choice of CE vs EE and respondants left some comments related to openess to receive more notifications about upgrading. We will conduct more research on the best way to notify users of upgrading and how to automate the upgrade process. The initial issue can be found [here](https://gitlab.com/gitlab-org/ux-research/-/issues/1377)
    - We will continue to iterate on how to [better highlight](https://gitlab.com/gitlab-org/gitlab/-/issues/295266) when instances are out of date to administrators.
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric helps us understand whether end users are actually seeing value in, and are using, geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  We've enabled [gathering usage data from Geo secondaries](https://gitlab.com/groups/gitlab-org/-/epics/4660) and have [added git fetch metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/298781) to the usage data. These metrics will be available starting with the 13.11 release. We also intend to add tracking for [git push operations on a Geo secondary](https://gitlab.com/gitlab-org/gitlab/-/issues/320984).
    - GMAU is currently based on logging into the Secondary web interface. Git access is more common and we will begin tracking Git pulls in 13.11 with tracking for Git pushes planned as part of the [Geo Usage Ping Epic](https://gitlab.com/groups/gitlab-org/-/epics/4660). We also know that the UX of the secondary web interface is not good and we want to remove it, see [Opportunity Canvas(internal)](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit#heading=h.4mt5fmtn0ax4). In order to assess the impact of our planned changes, having this (low) GMAU is really important. We've recently completed a PoC to make the secondary UI indistinguishable from the primary. Implementation work is in progress and can be tracked in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    -  For Disaster Recovery we are measuring *potential Geo users* - the number of active licenses. In an ideal world, no regular user would ever need to rely on Geo because there is no disaster, but if one occurs everyone benefits. Based on [the node number distribution](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=6471733&udv=0) ~60% of our customers use Geo mainly for DR. I think this is worth measuring because setting up Geo is always a conscious decision by the customer - it must be configured.
    - We are working to add support for replicating all data types, so that Geo is on a solid foundation for both DR and Geo Replication.[Over 80% of data types are now replicated](https://docs.gitlab.com/ee/administration/geo/replication/datatypes.html#limitations-on-replicationverification). [Replication for group wikis was shipped in 13.10](https://about.gitlab.com/releases/2021/03/22/gitlab-13-10-released/#geo-supports-replicating-group-wikis) and the Testing group plans to add [replication for Pipeline Artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/238464) in 13.11. 
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
  lessons:
    learned:
    - Read-only maintenance mode released in 13.9, a highly requested feature which makes it easier for system administrators to perform maintenance operations such as failovers.
    - While there was a large jump from January to February, the number is still very low right now. Two potential reasons a) data is incomplete b) WebUI is read-only and requires a different UI. We are planning to change this, see [this opportunity canvas](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit?usp=sharing) and in-progress implementation work in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    - Steady month over month growth with [4.7% increase in new Geo customers from Jan to Feb](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=9939914&udv=0).
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2    

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to adjust chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744) to better represent a default configuration.
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - Memory consumption has stabilized for the last four versions (13.7-13.10).
    - The high impact issues in [Running GitLab in a memory constrained environment](https://gitlab.com/groups/gitlab-org/-/epics/448) have been completed. As a followup, we have completed [benchmarking the memory consumption in a memory constrained environment](https://gitlab.com/gitlab-org/gitlab/-/issues/321565), and we have added documentation on [Running GitLab in a memory-constrained environment](https://docs.gitlab.com/omnibus/settings/memory_constrained_envs.html).
    - To further reduce memory consumption, we are planning to work on [splitting the application into functional parts to ensure that only needed code is loaded](https://gitlab.com/groups/gitlab-org/-/epics/5278) and on [reducing the memory consumption for Puma and Sidekiq endpoints](https://gitlab.com/groups/gitlab-org/-/epics/5622).

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Self-Manged and SaaS both continues to see significant increase in Paid GMAU, Up +103K since December 2020.  
    - We continue to see steady growth in the number of customers that enabled Advanced Search. [Usage Ping for Advanced Search](https://app.periscopedata.com/app/gitlab/596072/Enablement:-Search-Metrics?widget=7800022&udv=1037126)
  implementation:
    status: Complete
    reasons:
    - Data collected is incomplete for September and October 2020 and January 2021
  lessons:
   learned:
    - Improvments to Global Search Documentation was identifed as the top request from customers. [Documentation got major revisions as part of 13.10](https://gitlab.com/gitlab-org/gitlab/-/issues/250270). 
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Apdex is stable and continuing to exceed the target, with the Apdex for EE having fully caught up with CE.
    - We expect the updates performed in the scope of [Research Database queries for performance and frequency](https://gitlab.com/groups/gitlab-org/-/epics/5652) to further improve the ratio of queries which complete within 250ms and lower the variance of the Apdex even further.
    - We expect our work on [automated database migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6) to indirectly improve the database performance by minimising the number of database related incidents during deployments.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/yxe4pv4a) and [50ms - Tolerable 100ms](https://tinyurl.com/y6latcuc)) but reflects with sharp drops the production incidents that related to the database.
    - The vast majority of GitLab 13.9 instances use PostgreSQL 12.5. Only 2.4% of EE instances are still in PostgreSQL 11.x.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 111750
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Paid Monthly Active Users on GitLab.com increased by 10% in February to 113K, surpassing our target of 111K.
    - Insight - The increase was driven by several expansion deals in the Premium tier. We saw a similar jump in paid GMAU from January to February last year.
    - Improvement - To influence Paid GMAU, we are focusing on improving uptime and stability of GitLab.com via the Rapid Action process, .com daily standup, and prioiritizing InfraDev related issues above new customer requests. Additionally, we are working with the Manage stage to eliminate [key feature gaps](https://about.gitlab.com/direction/manage/#gitlabcom) that exist for Adminstrators on .com compared to self-managed, and extending our [compliance posture](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/2505) on Gitlab.com.
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  lessons:
    learned:
    - Need to account for seasonal fluctuations into target projections. 
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2
