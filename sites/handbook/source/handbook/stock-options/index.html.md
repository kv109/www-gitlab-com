---
layout: handbook-page-toc
title: "Stock Options"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
## About Your Ownership in GitLab
 
At GitLab we strongly believe in employee ownership in our Company. We are in business
to create value for our shareholders and we want our employees to benefit from that shared success.

*In the [compensation calculator](https://comp-calculator.gitlab.net/) (only accessible to GitLab team-members and candidates), you can find some more details on the most recent valuations as well as potential valuations.*

This guide is meant to help you understand the piece of GitLab that you’re going to own!
Its goal is to be more straightforward than the full GitLab 2015 Equity Incentive Plan
(the “2015 Equity Plan”) and your stock option agreement which you are advised to read,
which both go into the full legal details.  Please note, however, that while we hope that
this guide is helpful to understanding the stock options and/or stock issued to you under
the 2015 Equity Plan, the governing terms and conditions are contained in the 2015 Equity
Plan and the related stock option agreement. You should consult an employment attorney
and/or a tax advisor if you have any questions about navigating your stock options and
before you make important decisions.
 
Three things must happen for your stock options to be meaningful:
 
1. You must vest the stock (we have a 1 year cliff and 3 years of vesting after that).
2. You must stay until we have a liquidation event (or the lock-up period passes in the event we go public), or you have the cash to [exercise your stock after termination](#exercise-window-after-termination).
3. We must make the company worth more than the liquidation preference.
 
## Stock Options
 
At GitLab, we give equity grants in the form of Incentive Stock Options (ISOs) and
Non-Qualified Stock Options (NSOs). The difference in these two types of grants are,
generally, as follows: ISOs are issued to US employees and carry a special form of
tax treatment recognized by the US Internal Revenue Service (IRS). NSOs are granted
to contractors and non-US employees. It’s called an option because you have the option
to buy GitLab stock later, subject to vesting terms, at the exercise price provided at
the time of grant. Solely for the purposes of example, if you are granted stock options
with an exercise price of $1 per share of common stock today, and if GitLab grows later
so its common stock is worth $20 per share, you will still be able to buy the common
stock upon exercise of your option for $1 per share.
 
The reason we give stock options instead of straight stock is that you do not need to
spend any money to purchase the stock at the date of grant and can decide to purchase
the stock later as your options vest. In addition, we do not provide straight stock grants
since this may subject you to immediate tax liabilities. For example, if we granted you
$10,000 worth of GitLab stock today, you would have to pay taxes on the value of the stock
(potentially thousands of dollars) for this tax year. If we give you options for $10,000
worth of stock, you generally don’t have to pay any taxes until you exercise them (more
on exercising later). Please read the Stock Option section of the [Tax Team](/handbook/tax/#tax-procedure-for-reporting-taxable-gains-on-option-exercises).
 
## FY22 Stock Option Grant Plan Design
 
Stock options may be available to a GitLab team member as part of their overall [Total Rewards package](/handbook/total-rewards/).
The stock option ranges by role are calculated as follows:
* Minimum Number of Stock Options:
   * Minimum of Compensation Range x Equity Percent x Stock Grant Assumption
* Median Number of Stock Options:
   * Median of Compensation Range x Equity Percent x Stock Grant Assumption
* Maximum Number of Stock Options:
   * Maximum of Compensation Range x Equity Percent x Stock Grant Assumption
 
**Definitions:**
* [Compensation Range](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator-formula) is the cash compensation range as seen in the GitLab [Compensation Calculator](https://comp-calculator.gitlab.net/users/sign_in). 
* [Equity Percent](https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/-/blob/master/data/equity_calculator.yml) is a globally consistent percentage of compensation range based on the team member’s job grade.
   * The Equity Percent by grade will be updated annually in the Compensation Calculator and linked once merged.
* [Stock Grant Assumption](https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/-/blob/master/data/equity_calculator.yml), which will be calculated quarterly by GitLab, is typically the spread between the price of the Company’s preferred stock and its most recent 409a valuation.
   * The Stock Grant Assumption will be updated quarterly in the Compensation Calculator and linked once merged.
 
Note: All stock option grants are subject to approval by the Board of Directors and no grants are final until such approval has been obtained. The company reserves the right in its sole discretion to make any adjustments to stock option grants including the decision not to make a grant at all.
 
## New Hire Grants
 
New hire grants use the formula above multiplied by a New Hire Multiplier.
The New Hire Multiplier is typically 2, but may vary.
 
If you have any questions on what grant should be offered to a new hire, please reach out to the Total Rewards team by email at `total-rewards@gitlab.com`.
 
### Refresh Grants
 
As part of the updated [Annual Compensation Review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) process, eligible GitLab team members will be reviewed for a refresh grant once per year. Refresh grants use the formula above to determine the range of stock options a team member may be eligible for in that cycle based on their current role/grade. Refresh grants will [vest](/handbook/stock-options/#vesting) in accordance with the equity plan agreement between GitLab and the team members.
 
Note: All stock option grants are subject to approval by the Board of Directors and no grants are final until such approval has been obtained. The company reserves the right in its sole discretion to make any adjustments to stock option grants including the decision not to make a grant at all.
 
#### Annual Refresh Grant Program Eligibility
 
Team members may be eligible to participate in the annual refresh grant cycle review if they have completed six months of service with the company. However, participation will be based upon individual team member performance (including if a team member is currently in good standing) and [key talent criteria](https://about.gitlab.com/handbook/people-group/talent-assessment/#key-talent-criteria). Therefore, not all team members will receive a refresh grant in the annual review cycle. Similar to our current annual cash compensation review process, leaders will have discretion on the size of the refresh grant based on performance and equity budget allowance as allocated to each E-group member’s division. All proposed grants will be reviewed by the People team for pay equality. All proposed stock option grants are subject to review and approval by the GitLab Board of Directors.
 
#### FY22 Annual Refresh Grant Cycle
 
GitLab has completed its first annual refresh grant program in Q1 FY22. In order to move GitLab team members from a 3.5 year refresh cadence to an annual refresh cadence, this fiscal year, due to the change in cadence of this process, a one time transition multiplier was applied to all eligible team members based on time since the last new hire or refresh grant. The cut off date for this multiplier was March 17th. This transition multiplier will not be used going forward in future annual refresh cycles.
 
##### Transition Multiplier
 
| Time Since Last Grant  | Transition Multiplier |
|------------------------|-----------------------|
| 3+ years               | 2.00x                 |
| 2.5 - 3.0 years        | 1.75x                 |
| 2.0 – 2.5 years        | 1.50x                 |
| 1.5 - 2.0 years        | 1.25x                 |
| < 1.5 years            | 1.00x                 |
 
 
##### Key Talent
 
Key Talent (~10% of overall population) was identified during the FY22 Equity Refresh program. For more information, please refer to [the Key Talent section](https://about.gitlab.com/handbook/people-group/talent-assessment/#key-talent-criteria) on the Talent Assessment handbook page.
 
##### Timeline

| Date | Action |
|------|--------|
| 2021-03-22 | [Compensation Calculator](https://comp-calculator.gitlab.net/) updated to include equity range calculator. |
| 2021-03-24 | Grant Amount & Total Rewards Statements Uploaded to BambooHR (stock options table turned off in BambooHR for self-service). |
| 2021-03-25 to 2021-04-09 | Leaders/Managers to notify team members of grants and share Total Rewards Statement in BambooHR. |
| 2021-04-12 | Equity Grants added to Carta, access to stock options table in BambooHR turned back on. |

#### FAQs
 
Q: What happens to my existing awards?

Awards granted prior to FY22 will stay the same. The annual equity refresh changes will only apply to any new equity grants you may receive.
 
Q: What is the vesting schedule for new equity grants?

New equity grants will follow the same four year vesting schedule, with a one year cliff, then vesting monthly thereafter that is currently in the handbook.
 
Q: How will the new equity percentages be determined?

To determine a fair and equitable stock option percentage, we have and will continue to carefully look at market benchmarking to make sure that option grants are competitive. GitLab uses an external equity consulting firm for this analysis and to verify GitLab’s model.
 
Q: If I receive an equity refresh grant, is it based on my latest performance review?

In most cases, your equity refresh grant will be based on your latest performance review. However, in some cases, your performance is not strictly based on your most recent performance review. For instance, someone may have been deemed “developing” because they were recently promoted to a new level. They may be “performing” or “exceeding” for the equity review process on account that they were rated “developing” solely due to their recent promotion. A team member  may also be considered “key talent” in which case that will also factor into their equity refresh grant.
 
Q: How is key talent determined?

Managers will use their discretion to determine key talent (~10% of overall population). Key talent is defined as:
* Critical disruption to product if they were to leave
* Critical disruption to ARR (including delays for customers) if they were to leave
* Negatively impact company ability to achieve significant milestones
* Key impact to significant process (may be a single source of failure)
 
Q: Will everyone receive a refresh grant?

In the FY22 Equity Review cycle, not everyone received an annual refresh grant. It is important to note that under the new program, the amount of annual refreshes will be based on your performance, role, and location. In our performance-based culture, we want to incentivize team member’s performance by recognizing team member contributions to GitLab. As these factors will be reviewed annually, even if you do not receive an annual refresh one year, that does not affect your ability to potentially receive a grant in following years.
 
Q: Will the equity grants be equitable?

The GitLab Total Rewards team conducted a pay equality analysis to ensure fairness in decision making.
 
Q: How are the dollar ranges converted to a # of options?

Based on the value of the stock options using the Stock Grant Assumption price.

Q: When will the next equity refresh cycle be?

Moving forward, we will align the annual equity review with our [annual compensation review](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review), which has historically occurred in Q4.  
 
### Promotions
 
Promotion grants are based on the differential between the new hire stock options at the new grade minus the new hire stock options at the current grade using the median of the compensation range. The vesting schedule for your new stock option grant will align the date of your promotion.
 
Formula for number of Promotion Stock Options: `((New Role Median x Equity Percent x 2) - (Current Role Median x Equity Percent x 2)) / Stock Grant Assumption`
 
### Potential Value Scenarios
 
The value of the stock options granted depends on potential value scenarios which can be seen using the Equity section of the Compensation Calculator. 
 
## Vesting
 
Vesting means that you have to remain employed by, or are otherwise a service provider
to, GitLab for a certain period of time before you can fully own the stock purchased
under your stock option. Instead of giving you the right to purchase and own all of
the common stock under your stock option on day one, you get to own the stock under
your stock option in increments over time. This process is called vesting and different
companies offer vesting schedules of different lengths. At GitLab, our standard practice
is to issue options with a four year vesting schedule so you would own a quarter of your
stock after 12 months, half of your stock after two years, and all of it after 4 years.
Vesting happens on a monthly basis (so you vest 1/48 of your options each month), but
many vesting schedules include a cliff. A cliff is a period at the beginning of the
vesting period where your equity does not vest monthly, but instead vests at the end of
the cliff period. At most companies, including GitLab, this cliff period is generally
one year. This means that if you leave your job either voluntarily or involuntarily
before you’ve worked for a whole year, none of your options will be vested. At the end
of that year, you’ll vest the entire year’s worth (12 months) of equity all at once.
This helps keep the ownership of GitLab stock to folks who have worked at the company
for a meaningful amount of time.
 
## Dilution
 
This section deals with dilution which happens to all companies over time. In general
companies issue stock from time to time in the future. For example, if company XYZ needs
to raise money from outside investors, it may need to create new stock to sell to those
investors. The effect of additional stock issuances by company XYZ is that while you
will own the same number of shares as you did before such issuance, there will be more
total shares of outstanding and, as a result, you will own a smaller percent of the
company -- this is called dilution.
 
Dilution does not necessarily mean reduced value. When a company raises
money the value of stock will stay the same because the company’s new valuation will be equal
to the old value of the company + the new capital raised. For example, if company XYZ
is worth $100m and it raises $25m, company XYZ is now worth $125m. If you owned 5%
of $100m before, you now own 4% of $125m (20% of the company was sold, or, said differently,
your 5% stake was diluted by 20%). The 5% stake was worth $5m before the fundraising, but the 4% stake is still worth $5m after.
 
Funding rounds can have both primary components and secondary components.
Primary components result in dilution as described here, but secondary rounds
are a bit different. Rather than new shares being issued, participants in a
secondary exchange their funding for existing shares. You can read more in
[this Quora article](https://www.quora.com/What-is-a-secondary-component-to-a-VC-round/answer/Elad-Gil).
 
## Stock Splits
 
Companies from time to time undertake stock splits. A stock split has no economic impact on the stockholder. The split simply increases the number of shares by a certain amount and reduces the price by an equal, offsetting amount. For example, if a stockholder had 1,000 shares at $10.00 per share (total value $10,000), and the Company executed a 2 for 1 stock split, the shareholder would then have 2,000 shares at $5.00 (total value still $10,000). Nothing has changed. Public companies have historically split their stock to lower the stock price so that a broader set of investors could hold shares in the company. The theory being a lower price would make it easier for an individual investor to buy shares. Private companies perform stock splits to make themselves more comparable to other private companies. Companies that undertake IPOs typically go public in accepted trading ranges and then start trading from that point. So private companies will adjust their shares to be able to trade in those ranges at IPO. For further information, read [this article](https://smartasset.com/investing/what-is-a-stock-split).
 
Effective February 28, 2019 the GitLab Board of Directors approved a 4:1 stock split. All common stock, preferred stock and options were treated exactly the same in the split. The stock split will be reflected in Carta by the end of April. Why 4:1? We chose that ratio so that our shares will be comparable to other companies in our peer group, so that candidates considering coming to work at GitLab can make an informed choice on an “apples to apples” basis. We also took care to not split at too high of a ratio which could result in a [reverse stock split](https://www.bizjournals.com/sanjose/stories/2001/01/15/focus3.html) prior to IPO and that is something we would like to avoid (but can't guarantee in any scenario).
 
## Exercising Your Options
 
"Exercising your options" means buying the stock guaranteed by your options. You pay
the exercise price that was set when the options were first granted and you get stock
certificates back.
To give employees an opportunity to benefit from any existing tax incentives that may
be available (including under the US and the Dutch tax laws) we have made the stock
immediately exercisable. This means you can exercise your right to purchase the unvested
shares under your option to start your holding period. However, the Company retains a
repurchase right for the unvested shares if your employment or other services ends for
any reason. An early exercise of unvested stock may have important tax implications and
you should consult your tax advisor before making such decision.
 
Also, while the company has the right to repurchase the unvested shares upon your
termination of services, the company is not obligated to do so. Accordingly you could
lose some or all of the investment you made. Because we are a young company there are
lots of risks, so be aware and informed of the risks. Please read this [quora thread about most startups failing](https://www.quora.com/What-is-the-truth-behind-9-out-of-10-startups-fail) and this story of [people paying more in tax for their stock than they get back](http://www.nytimes.com/2015/12/27/technology/when-a-unicorn-start-up-stumbles-its-employees-get-hurt.html).

### Trading Restrictions During Quarterly 409a Valuations

In order to prepare for quarterly 409a valuations, the right to exercise your stock options will be suspended for a period of time leading up to the effective date and Board approval of the new valuation.  

All exercise activity will be suspended one week prior to the effective date of the new valuation.  Carta will be disabled for US team members.  For non-US team members, paperwork and proof of payment/wire must be submitted by the last day to exercise.  The window to exercise will open the 1st business day following Board approval.  

The anticipated schedule is below and an announcement will also be made in Slack one week prior to the window closing, in an effort to keep team members aware.

| **Anticipated 409a Effective Date**       | **Anticipated Board Meeting (Approval)** | **Anticipated Last Day to Exercise** |
| :----------------------- | :--------------------------------------- | :----------------------------------- |        
| June 1, 2021             | June 17, 2021             	              | May 24, 2021	                       |
| September 1, 2021        | September 30, 2021                       | August 24, 2021   		               |
| December 1, 2021         | December 14, 2021                        | November 24,2021		                 |

### Option Expiration
 
If you leave the company, you will generally have [90 days to exercise your option](#exercise-window-after-termination)
for any shares that are vested (as of the last day of service).
 
In addition, if not otherwise expired (through termination of your employment), your stock
options expire 10 years after they were issued.
 
### Exercise Window After Termination
 
Please note that until the [post IPO lockup period](http://www.investopedia.com/ask/answer/12/ipo-lockup-period.asp) has expired (or we are bought) company stock is not liquid. If your employment ends
for whatever reason, you have a 90 day window to exercise your vested options, or lose them. During this window you would have to pay
the exercise price and in some cases the tax on the gain in value of your stock options, which could be considerable.
If the company stock is not liquid this money might be hard to come by. The 90 day window is an industry standard but
there are [good arguments against it](https://zachholman.com/posts/fuck-your-90-day-exercise-window/).
You may not purchase unvested shares after your service has ended.
 
At GitLab the stock options are intended to commit our team members to get us to
a successful public listing of our stock. We want to motivate and reward our people for reaching that goal. Therefore we will consider exercise
window extensions only on a case by case basis at our discretion. An example of a situation we'll consider is a valued
team member quitting because of personal circumstances. In most cases there will be no extension and you will either have
to pay for shares and the taxes yourself, or lose the options, even when you are fully vested. And, of course, [being a publicly listed company is our public ambition](/company/strategy/),
but neither the timing, nor whether it happens at all, is guaranteed.
 
If you leave GitLab after less than a year, your Carta account will stay active but all unvested GitLab shares (which will be all of them as our vesting schedule starts at 1 year) will show as expired and you will not be able to take any actions with those shares. At the exercise expiration date, since no options were exercised, all options will return to the option pool to be reissued by GitLab.
 
You can reach a Carta support rep by calling (650) 669-8381 but you will have to enter a support PIN that is associated with you. You can find this PIN by logging into your Carta account, clicking Help, then Contact Us and the PIN will be there.
 
## Administration
 
Option grants are approved by the Board of Directors at regularly scheduled quarterly board meetings. After your grant has been approved by the Board you will receive a grant notice by email from [Carta](https://carta.com/) containing information relevant to the grant including the number of shares, exercise price, vesting period and other key terms. We use Carta to administer our stock option program.
 
You will receive the grant notice to your GitLab email address. Clicking through that email will enable you to set up a user account at Carta. You can find all of the terms and conditions of the stock program as well as your specific grant within the Carta system. You are safe to go ahead and click "Accept" for your grant, no payment or taxes will be incurred to you since you are not [Exercising Your Options](#exercising-your-options), you are simply confirming to GitLab that you accept the grant that was allocated to you.
 
As a helpful hint we suggest that you add a second, personal email address to your profile. This can be added by clicking on Profile and Security in the bottom left hand corner of the home screen after logging in to Carta.
 
### How to Exercise Your Stock Options
 
There are two methods to exercise your shares:
 
1. Electronic (US Residents Only)
  - Log into your Carta account
  - Follow the directions to enable ACH payments from your bank
  - After ACH has been enabled select exercise option grants and follow the prompts
2. Manual (Non ACH and Non US Residents)
  - Log into your Carta account
  - On Portfolio Details, click the "Holdings" option
  - On the option grant list, click the arrow down icon on right side of the screen and select the "View" option
  - A modal will open. On the left side menu, click "Documents and notes"
  - Download the "Form of Exercise Agreement" file
  - Complete the form, sign, and return as PDF to `stockadmin@gitlab.com`
  - Send payment in US dollars by wire transfer. You will be provided the wire transfer info.
 
**Note for US residents:** whichever method you choose, if you early exercise unvested shares, be sure to download the **83-b election form** provided by Carta and file with the IRS within 30 days of exercise. Send a copy of the signed and dated election form to `stockadmin@gitlab.com`. The Stock Plan Administrator will file it within Bamboo/HR.
 
See on Carta: [What and where is my 83(b) form?](https://support.carta.com/s/article/what-and-where-is-my-83-b-form) or [download the 83-b election form template](https://carta.my.salesforce.com/sfc/p/#f4000000nrKS/a/0H000000Xm1Z/H0r8oAMKO54MeOPha6Y2ej8zlCnIrPzVyQsFP5XdboU)
 
You will most likely want to include the following letter when sending in the 83-b election to the IRS:
 
```
<<Date Filed>>
 
Department of the Treasury
 
<<Address provided from Carta 83-b instructions>>
 
To whom it may concern:
 
Please find enclosed two copies of the 83-b election in connection with my purchase of shares of GitLab Inc. common stock. Please return one copy stamped as received to my attention in the enclosed self addressed stamped envelope.
 
Yours Truly,
 
//signature
```
 
## Exercise Prices and 409A Valuations
 
Generally, the exercise price for options granted under the 2015 Equity Plan will be
at the fair market value of such common stock at the date of grant. In short, “fair
market value” is the price that a reasonable person could be expected to pay for the
common stock, but because GitLab is not “public” (listed on a large stock exchange),
the Board is responsible for determining the fair market value. In order to assist
the Board, the company retains outside advisors to undertake something called a “409A
valuation”. In general, the lower a valuation for the shares
the better for employees as there is more opportunity for gain. Additionally, a lower
exercise price reduces the cash required to exercise the shares and establish a holding
period which can have tax advantages in some countries. We describe those in this document
but as always check with your financial or tax advisor before taking any action.
 
## Taxes
 
Tax law is complex and you should consult a tax attorney or other tax advisor who is
familiar with startup stock options before making any decisions. Please go to the [tax team's stock options](/handbook/tax/stock-options/) page for more information on taxation of stock options.
 
### US employees with Incentive Stock Options (ISOs)
 
Taxation from the US perspective is not as straightforward as you might like. You aren’t taxed when you exercise
your options. Tax is due on the gain or profit you make when you sell the stock. Depending on your holding period,
the tax may be treated as ordinary income or capital gain. Moreover, when you hold the options long enough you may be subject to 0% capital gains tax. To outline the five possibilities of the different scenarios that may apply:
 
1. Exercise your options to purchase shares, and hold.
1. Exercise your options to purchase shares, and sell the shares within the same year.
1. Exercise your options to purchase shares, and sell the shares in less than twelve months, but during the following year.
1. Sell shares at least one year and a day after you exercised the options, but less than two years from the original grant date.
1. Sell shares at least two years from the original grant date.
 
Please note, however, that any gain upon exercise of an ISO (difference between the exercise price and fair market value
at date of exercise), even if you do not sell the shares, may be counted as a "tax preference"
towards the Alternative Minimum Tax limit. For instance, under scenario 1 above you have to make an adjustment in your tax return for the Alternative Minimum Tax (AMT) that equals the so-called bargain element. Each scenario has a different tax treatment, so be careful of the tax consequences when you exercise your options. In the long term, holding onto your stock does save taxes, however be aware of the AMT that you will be confronted with. It is strongly advised that you contact a tax advisor to be aware of the US tax consequences.
 
In addition to the benefits of a longer holding period, the IRS does have an additional benefit for holders of Qualified Small Business Stock (QSBS for short). GitLab meets the criteria for QSBS treatment for options exercised prior to August, 2018, however (again), the Company is not in a position to offer tax or legal advice nor does it make any representation about compliance with the QSBS provisions, so check with your own tax and financial advisors. We found [this article](https://blog.wealthfront.com/qualified-small-business-stock-2016/) helpful in describing the QSBS program in greater detail.
 
### US service providers with Nonqualified Statutory Options (NSOs)
 
For non-employees of GitLab that have been granted stock options, their stock options are treated as NSOs. NSOs have different tax treatments depending on whether they are actively traded on an established market:
 
- **when actively traded**: taxed at grant
- **when not actively traded**: taxed at exercise
 
Since GitLab's stock options are not actively traded on an established market, the NSO is taxed at exercise. The gain of the exercise (fair market value minus the exercise price) has to be reported by [form 1099-MISC (box 7)](https://www.irs.gov/pub/irs-pdf/f1099msc.pdf). Withholding is typically not required, however when the service provider fails to provide a valued tax identification number in form 1099, GitLab has to ensure backup withholding (roughly 25%).
 
### Netherlands, Germany and Australia
 
For our employees based in the Netherlands, Germany and Australia, the difference between the exercise price and the fair market value is considered taxable at the date you exercise your stock options. With respect to reporting taxes: the taxable gains are subject to employer tax withholding. The tax payable is therefore deducted from your gross payroll with respect to the exercise of your stock options.
 
### The United Kingdom
 
In the United Kingdom there is a small difference in the tax treatment of exercising your stock options as opposed to the other entities; the difference between the fair market value and the exercise price is taxed through payroll at the date of exercise if there is a liquid market for the stock at time of exercise. If there is no liquid market, income tax will likely still be due but the team member will need to settle this directly with HMRC. For more information please check [our memo on this](/handbook/tax/#international-territory).
 
## Dual-Class Stock
 
Often companies will create multiple classes of stock, with each class having different voting rights, in order to provide protection to the company's founders, early investors, and early employees, whose long-term vision for the company may not align with that of later stage investors. At the GitLab Board of Directors meeting held on January 31, 2019, the Board approved the creation of such a dual-class structure.
 
### Effect of Dual-Class Stock
 
- Class B stockholders retain significant influence over stockholder votes and actions.
- The Influence of long-term Class B stockholders increases as other stockholders from before being public sell or distribute their Class B stock, which coverts into Class A common stock upon sale or distribution.
 
### Dual-Class Voting
 
| Type                     | Detail             |
| :----------------------- | :----------------- |
| **Class A Common Stock** | 1 vote per share   |
| **Class B Common Stock** | 10 votes per share |
 
### Creation of Dual-Class Stock
 
**Step 1: Before being a Publicly Traded Company**
 
- Create two classes of common stock.
- Outstanding common stock converts to Class B common stock.
- Options and RSUs subsequently issued exercise into Class A common stock.
 
**Step 2: When Going Public**
 
- Preferred stock converts to Class B common stock.
- Class A common stock sold in public offering.
 
**Step 3: Being Public**
 
- Class B converts to Class A in sale or distribution unless “permitted transfer” (see below).
- Additional sales of Class A common stock.
- Permitted transfers would be transfers to one or more family members, transfers to a trust for the benefit of the stockholder or in favor of a family member of the stockholder, or transfers to a general partnership, limited partnership, limited liability corporation, or other entity controlled by the stock holder or a family member of the stockholder.
 
### Sunset
 
The stock structure will automatically convert and become a single-class structure upon (whichever comes first):
 
- A date specified by a vote of 66.66% of the outstanding shares of Class B common stock
- Death or Permanent Disability of Sid Sijbrandij
- At such time as when the Class B represents less than 5% of the outstanding Common Stock
 
## Transfer Restrictions on Common Stock
 
On January 31, 2019 the Board of Directors approved the amendment to the company's bylaws regarding the transfer of shares of Common Stock. Effective as of that date, Stockholders will not be able transfer, sell, or assign any shares of Common Stock without the prior written consent of the Board. This restriction does not apply to the following permitted transfers:
 
- the transfer is a gift or pursuant to a domestic relations order, to the stockholder's immediate family member;
- the transfer is executed pursuant to the stockholder's will or the laws of intestate succession;
- the transfer by an entity stockholder is made to an affiliate of such entity stockholder;
- the transfer by an entity stockholder of all Common Shares is made to a single transferee in accordance with the terms of any bona fide merger, consolidation, or acquisition;
- the transfer is made for no consideration by a stockholder that is a partnership to such stockholder's limited partners in accordance with the partnership interests of such limited partners;
- any repurchase or redemption of the Common Shares by the corporation (a) at or below cost, upon the occurrence of certain events, such as the termination of employment or services, or (b) at any price pursuant to the corporation's exercise of a right of first refusal to repurchase such Common Shares; or
- in the event of a transfer or deemed-transfer that is approved, or in the event the restriction is waived, and the Common Shares of the transferring stockholder are subject to co-sale rights, any transfers by the persons and/or entities who are entitled to and have exercised the co-sale rights in conjunction with such approved transfer or deemed-transfer giving rise to the exercise of such co-sale right.
 
## Questions?
 
Everyone is always welcome to ask our CFO any questions they have about their options,
GitLab’s fundraising, or anything else related to equity at GitLab. However, everyone should
also consult a lawyer before making important financial decisions, especially regarding
their equity because there are complex legal and tax requirements that may apply.
 
### References
 
Our team member Drew Blessing [wrote about what he learned about stock options](http://blessing.io/startups/stock-options/2016/02/15/navigating-your-stock-options.html) after starting to research them because he received them when joining us. His article is greatly appreciated, but any advice is his own, and it is not officially endorsed by GitLab Inc.
 
## Stock Options Board Meeting Reporting
 
Each quarter before the board meeting, the Total Rewards Analyst will run a report for the CFO outlining the options pending approval. The process should be:
 
1. Run the Stock Options Report from BambooHR.
1. In the Stock Options Folder, create a new Google sheet titled "Stock Options as of YYYY-MM-DD".
1. Paste the BambooHR Report into a new tab in the sheet for historical reference.
1. Add new tabs for options that have already been approved (they will be denoted by Yes, No, or N/A).
1. Any blank "Approved by the Board" options should be added to a new sheet.
    * Audit options that have been approved by the board, but fall into the current board approval date range. For example, if the cut off for the last board meeting was November 20 and this board meeting is March 5, any grants that indicate approved with an effective date during this time span should be reviewed to ensure they were on a previous approval.
1. Use the pending options to population the "Option Grant" google doc by creating a new tab with the date of the board meeting.
1. Update the Fully Diluted Shares as shown in Carta and ensure all formulas are functioning properly.
1. Copy and paste the information from the pending approval tab to the proper column.
1. Enter ISO or International as the option type (ISO is for US employees).
1. Enter country for each participant.
1. Enter Department for each participant.
1. For low/high this should reflect the highest and lowest number of options someone in that level was contracted. Leave this blank for promotion.
1. Run a report on [refresh grants](/handbook/stock-options/#refresh-grants) and add these to the sheet. Note reporting on refresh grants is only applicable for the board meeting directly following an equity refresh review cycle.
1. Ping the CFO that the report is ready for review.
1. Verify all options were approved (or not) with the CFO after the board meeting and change the approval in BambooHR to avoid duplications on the next report.
 
### Audits

**Job Grade (Promotion) Audit**
1. Pull a historical report of job grades in BambooHR.
1. Identify any changes in job grade between the last board meeting cut off date and the current cut off date.
1. Reconcile that each team member who had an increase in grade during this time period has been granted stock options and included on the board meeting report.
    * If anyone who received an increase in grade, but isn't in the board meeting report: review the original promotion request in BambooHR or Greenhouse for a grant. If not in either of these systems, check whether the grant was missed in the promotion process. If here, process in BambooHR and the report accordingly.

**Previous Board Meeting Audit**
1. Pull a list of all grants in BambooHR with effective dates within the cutoff dates of the last board meeting. 
1. Compare against the previous board meeting's list of grants that were reviewed and approved. 
1. Ensure all grants that were not included/approved in the previous board meeting have not been marked approved by the board in BambooHR and are included in the current summary for review and approval. 

## Procedures for Issuing Options to Team Members
 
1. In Carta Download the Bulk Import Sheet.
1. Cut and paste the Option Grant sheet approved by the Board into the Bulk Import Sheet.
1. Use the concatenate function to merge team member names into a single cell.
1. Designate options as ISO, NSO or INTL. Carta will automatically convert ISOs over the limit into NSOs.
1. Issue date relation - Enter all team members, advisors and board members as Employees. GitLab early adopted ASU 2018-07 (w.r.t accounting for stock options issued to non-employees) which is why we designate all team members as employees.
1. The standard vesting schedule is 1/48 monthly with a 1 year cliff.
1. Enter YES for Early Exercise.
1. Check with Fenwick on Federal and State Exemption whether Rules 701 and 21052(f) remain applicable.
1. Ensure any non-standard vesting terms are correctly input on bulk import or individually in Carta.
1. Document Set is "2015 Equity Incentive Plan".
1. All other optional fields can be left blank as default settings have been pre-populated in Carta.
1. When successfully uploaded make announcement in Team Call. All hires and promotions through xx date; all refresh grants for hires through yy date.
 
## Procedures for Adding Team Members Department/Cost Center in Carta
 
1. Once the new awards have been loaded and approved, the team members’ department needs to be added manually to Carta.  Note: This is only being done for team members, not Investors.
1. The headcount report is downloaded from Bamboo for all team members.
1. In Carta, choose “Stakeholders - All Stakeholders”
1. Sort on the “Cost Center” to move those with an empty field to the top of the group
1. Click the down arrow to the far right of the team member record/row, and choose “Edit Stakeholder Properties”
1. In the new window, make sure you’re in the “Cost Center” and enter the effective date (using last grant date) and the cost center according to the headcount report, in exact form - and save.
1. Make sure this is done for all team members that have a blank cost center field.
 
## Procedures for Terminating a Team Member in Carta with share repurchase
 
1. In Carta, go to -> Stakeholders -> All Stakeholders -> and search for team member
2. Far right of the line, in the drop down choose “Terminate Stakeholder”
3. Begin entering termination information
- Termination Date
- Termination Type = Voluntary or Involuntary
- New Relationship = Ex-employee
- Notifications - Add a new email, which is the personal email address and can be found under their profile
4. For shares being repurchased:  Confirm the dates to exercise and repurchase are correct and check “I confirm the last day to repurchase is correct for each certificate” and save the termination
5. To repurchase a certificate, navigate to 'Shares' under Securities. Then select 'Repurchase shares' in the drop-down menu for the appropriate tranche.
6. Enter the certificate repurchase information (date, number of shares, transaction value) and make sure the “Transfer funds via ACH” toggle is on. Next: review repurchase details
7. Review the details of the repurchase (shares, value, date) and confirm repurchase
8. The final screen will confirm the total funds that will be withdrawn from the company account and transferred to the team member for the repurchase of the shares.
9. Send email to team member bcc'ing the CFO and Peopleops.  A sample email is below:
- “Hello,[team member], just to confirm that GitLab has initiated a repurchase of your unvested shares.  The transaction has been processed through Carta.  You should expect to see an ACH transaction into your bank account in the amount(s) of $xxx.xx (and $x,xxx.xx, if necessary).  Please contact me if you have any concerns or questions.”
10. Send to their personal email address
11. File email in GitLab Inc=> Equity => Repurchases
12. Send note to Accounting and External Reporting, and get screenshot of transfer out of Comerica
13. File screen shot as above.
14. Once all steps are completed, ensure the offboarding issue is marked as such by checking the box for Carta, assigned to the Stock Plan Administrator
 
## Procedures for Processing a Manual Exercise
 
1. After the company has received the signed exercise agreement and the payment from the team member, the Stock Plan Administrator will compare the form and calculate the exercise costs to ensure the funds received are accurate.
2. The Stock Plan Administrator will request a screenshot of the payment received and save it to the “Equity Cash Receipts - Temporary” folder.
3. The exercise form will be sent to the CFO via HelloSign with request for signature.  Once signed, the exercise form and cash receipt will be downloaded to the “Equity - Option Exercise Agreements” folder.
4. Once the documents are ready to load, the Stock Plan Administrator will process the exercise in Carta.
5. The signed exercise form will then be uploaded to the team members Carta account for record keeping.
6. Navigate to the appropriate option grant in Carta and select 'Exercise options' in the drop-down menu.
7. Fill out all pertinent details about the exercise date and the number of options to be exercised. Then click 'Next: review certificate details.'
8. The newly issued certificate now shows up under the common certificate section of the cap table.
 
## Stock Administrator Performance Indicators
 
### Number of award transactions processed
 
The number of award transactions processed in Carta over a quarter. This is measured on the last calendar day of the quarter. The target has not been determined.
 
### Number of participants supported
 
The number of participants supported is measured on the last day of the calendar month.
 
## Equity Knowledge Assessment
 
You can test your knowledge on our equity by taking the [GitLab Equity Knowledge Assessment](https://docs.google.com/forms/d/e/1FAIpQLSe2SgyY8Ndfam39-2LOhgJf3ySgvml2zDdrStFuSCVbBUBQoA/viewform).
 
If you have questions about equity or the content in the Knowledge Assessment, please reach out to the Total Rewards team. If the quiz is not working or you have not received your certificate after passing the Knowledge Assessment, please reach out to the Learning & Development team at `learning@gitlab.com`.
