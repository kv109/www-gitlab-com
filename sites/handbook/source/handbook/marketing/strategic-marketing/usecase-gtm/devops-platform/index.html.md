---
layout: markdown_page
title: "Use Case: DevOps Platform"
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

#### Who to contact

| PMM | TMM |
| ---- | --- |
| Brian Glanz @brianglanz and Cormac Foster @cfoster3 | William Arias @warias |

# The Market Viewpoint

## DevOps Platform

The  DevOps Platform use case showcases the benefits of a single, end-to-end platform for DevSecOps--from ideation to value creation and delivery, with complete transparency, traceability, and seamless collaboration throughout the process. It is relevant to customers who have not been able to achieve expected results due to maintaining a hodgepodge of tools and integrations, siloed teams, cultural misalignment, lack of visibility and/or collaboration. In such scenarios, the **GitLab value of a single application** from idea to production inclusive of monitoring and security is appealing to the customer.

The DevOps Platform is also a vehicle for transformation, allowing businesses to not just reduce the overhead of delivering software, but to anchor initiatives to change the way the company approaches work and collaboration.

The [worldwide DevOps market](https://www.researchandmarkets.com/reports/4856237/devops-market-global-industry-trends-share) is expected to grow with a **growth rate of 20 percent** from $3.5 Bn in 2018 to **$10.5 Bn in 2024**. Increasing adoption of agile frameworks, cloud technologies, and digitization of enterprises to automate business processes are driving the growth. Furthermore, other solution providers and industry analysts have begun to embrace the concept of a DevOps Platform.

In their [Market Guide for DevOps Value Stream Delivery Platforms](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html), Gartner's Strategic Planning Assumption was:

**“By 2023, 40% of organizations will have switched from multiple point solutions to DevOps value stream delivery platforms to streamline application delivery, versus less than 10% in 2020.”**

<sub>[Market Guide for DevOps Value Stream Delivery Platforms](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html), Manjunath Bhat, Hassan Ennaciri, Chris Saunderson, Daniel Betts, Thomas Murphy, Joachim Herschmann, 28 September 2020</sub>

<sub>Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner's research organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.</sub>

GitLab believes this indicates that the market for a DevOps platform delivered as a single application will grow much faster than the DevOps market as a whole.

<iframe width="100%" height="500" src="https://www.youtube.com/embed/-_CDU6NFw7U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## The DevOps Platform use case vs. "Open DevOps Platform" brand messaging

While they share a similar name, GitLab's "Open DevOps Platform" brand messaging and the "DevOps Platform" use case are distinct and independent.

"Open DevOps Platform" is high-level brand messaging that describes our company, our product philosophy, and the relationship between the two.  It is focused on the positive, transformational outcomes of enaging with the company and using the product. 

"DevOps Platform" is a [customer use case ](https://about.gitlab.com/handbook/use-cases/#customer-use-case-definition) that describes a discrete customer problem that GitLab solves. The DevOps Platform use case is more targeted and narrowly scoped than the Open DevOps Platform brand messaging, focusing on explaining the benefits of a complete platform, deployed as a single application, addressing specific [Market Requirements](#market-requirements) with [product-specific differentiators](#how-gitlab-meets-the-market-requirements).

As an example of overlap between the brand messaging and the customer use case, consider that extensibility is one of the [Market Requirements](#market-requirements) of the solution. "How GitLab delivers" that includes a couple aspects of what "Open" means: 

> Dozens of [project integrations](https://docs.gitlab.com/ee/user/project/integrations/overview.html), Webhooks, and an [open core](https://about.gitlab.com/company/stewardship/#business-model) model in which everyone can [contribute directly](https://about.gitlab.com/company/stewardship/#contributing-an-existing-feature-to-open-source-it) to the GitLab project.

As an example of the distinction between the two, consider our public roadmap and handbook.

- The fact that we open source our business the same way we open source our code is a core element of our Open DevOps Platform brand message. 
- Having a public roadmap or handbook is not required to use GitLab, and has little to do with the value GitLab brings to customers when they use it as their DevOps Platform. 

While the two motions serve different audiences and purposes, they do share a large amount of common messaging, as the Visibility, Collaboration, and Velocity pillars of the Open DevOps Platform (and the resulting outcomes of Iteration and Innovation) are also important to the DevOps Platform solution. 



## Personas

### User Persona
There are two primary user personas:

1. [Devon - the DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
Devon assists multiple different teams with varied needs. A single DevOps platform provides a consistent and efficient development experience, eliminating the burden of supporting unique, team-specific implementations.

1. [Delaney - the Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
Delaney is responsible for communication and collaboration throughout the entire product lifecycle, and requires end-to-end visibility into work items and status to accurately estimate capacity, plan features, and coordinate with all parties. A single DevOps platform provides those services natively, allowing Delaney to access necessary information as needed, without dependencies.


### Buyer Personas
End to end DevOps requirements typically involve executive involvement - VP of DevOps, VP of Engineering, VP of Innovation and above.

The two primary buyer personas are:

1. [Erin - the Application Development Executive](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc)
The consistency of a DevOps platform and the productivity gains of transparency and contextual aggregation of relevant information align to her primary goal of *predictable business results*.
1. [Kennedy - the Infrastructure Engineering Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director)
For Kennedy, the most important benefit of a DevOps Platform delivered as a single application is a decreased cost-to-performance ration while enhancing security and compliance.

### Relevant Market Segments

The DevOps Platform is relevant to all market segments and industries, though different aspects of the DevOps Platform will resonate with each group. The <a href="/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/message-house/">Message House</a> provides detailed information on segment messaging and positioning.



## Analyst Coverage

- [Gartner - Market Guide for DevOps Value Stream Delivery Platforms](https://www.gartner.com/document/3991050)
- [Gartner - Hype Cycle for Agile and DevOps, 2020](https://www.gartner.com/document/3987588)
- [Forrester - Predictions 2020: DevOps](https://www.forrester.com/report/Predictions+2020+DevOps/-/E-RES157594)
- [Forrester - The Rise, Fall, And Rise Again Of The Integrated Developer Toolchain](https://go.forrester.com/blogs/the-rise-fall-and-rise-again-of-the-integrated-developer-tool-chain/)
- [Research & Markets - DevOps Market 2019-2024](https://www.researchandmarkets.com/reports/4856237/devops-market-global-industry-trends-share)


## Market Requirements

| Market Requirement | Description | Typical capability-enabling features | Value/ROI |
|---------|-------------|-----------|------|
| 1. Agile Management | The solution supports the planning, initiating, monitoring, controlling, closing, and measuring the value created by Agile teams and projects. | Requirements, Epics, Features, Stories, Iterations, Backlogs, Roadmaps, Boards | Allows businesses to budget for, prioritize, deliver, and assess the success of innovation initiatives. |
| 2. Version Control & Collaboration | Control and manage different versions of the application assets from code to configuration and from design to deployment. | Typical capability-enabling features | Value/ROI |
| 3. Continuous Integration | Automate and streamline build and test to improve quality and velocity. | Build automation, test automation, pipeline configuration management, visibility & collaboration, multi-platform and language support, pipeline security, and more ([see full list](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/ci/#market-requirements)) | CI makes software development easier, faster and less risky for development teams. |
| 4. Security | Shift security left and make it relevant throughout the delivery lifecycle. | Typical capability-enabling features | Value/ROI |
| 5. Continuous Delivery | Streamline and automate delivering and deploying code to different environments. | Progressive Delivery, Roll out scenarios, Feature Flags, Review Apps, Post Deployment Monitoring | Consistent and repeatable release processes, faster time to market and lower risk releases |
| 6. Governance, Risk Management, and Compliance | Define, enforce and report on compliance policies and frameworks | Requirements Management, Policy Management, Audit Management | Value/ROI |
| 7. Incident Management | Organize multiple inputs into actionable workflows to allow the appropriate people to remedy anomalous conditions.  | Typical capability-enabling features | Value/ROI |
| 8. Extensibility | Extend the system to work with business applications, external data sources, and legacy point solutions.  | Webhooks, APIs | Customize DevOps platform to any workflow or business need, adopt end-to-end DevOps incrementally |

# The GitLab Solution

## How GitLab Meets the Market Requirements

| Market Requirements | How GitLab Delivers | GitLab **Stage**/Category | Demos |
|---------|-------------|-----------|------|
| 1. Agile Management | Project, program, and portfolio management within a single system, enhanced by complete visibility into the work being done and the efficiency of value delivery. | [**Plan stage:**](/stages-devops-lifecycle/plan/) [Epics](https://docs.gitlab.com/ee/user/group/epics/), [Iterations](https://docs.gitlab.com/ee/user/group/iterations/), [Milestones](https://docs.gitlab.com/ee/user/project/milestones/), [Requirements Management](https://about.gitlab.com/direction/plan/requirements_management/).<br>[**Manage stage:**](/stages-devops-lifecycle/manage/) [Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#value-stream-analytics), [Insights](https://docs.gitlab.com/ee/user/project/insights/#insights). | -- |
| 2. Version Control & Collaboration | -- | -- | -- |
| 3. Continuous Integration | Architect CI pipelines with .gitlab-ci.yml files, structure CI processes, run automated tests, security scans, and build your app using GitLab Runner as the execution agent. See code quality analysis and code coverage details from source code. Get feedback on code changes directly in GitLab. Manage packages, repositories, and containers along with their dependencies in GitLab. See full list [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/ci/#how-gitlab-meets-the-market-requirements) | [Verify](https://about.gitlab.com/stages-devops-lifecycle/verify/), [Package](https://about.gitlab.com/stages-devops-lifecycle/package/), and [Release](https://about.gitlab.com/stages-devops-lifecycle/release/), and [Secure](https://about.gitlab.com/stages-devops-lifecycle/secure/) | -- |
| 4. Security | -- | -- | -- |
| 5. Continuous Delivery | Unified, automated deployment and monitoring strategies with modern compliance within a single application | [**Release Stage**](https://about.gitlab.com/stages-devops-lifecycle/release/): [Continuous Delivery](https://about.gitlab.com/product/continuous-delivery/), [Review Apps](https://about.gitlab.com/product/review-apps/), [Advanced Deployments](https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production), [Feature Flags](https://docs.gitlab.com/ee/operations/feature_flags.html), [Release Evidence](https://docs.gitlab.com/ee/user/project/releases/#release-evidence), [Secrets Management](https://docs.gitlab.com/ee/integration/vault.html) <br> [**Monitor Stage**](https://about.gitlab.com/stages-devops-lifecycle/monitor/): [Metrics](https://docs.gitlab.com/ee/operations/metrics/), [Logging](https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html), [Tracing](https://docs.gitlab.com/ee/operations/tracing.html), [Error Tracking](https://docs.gitlab.com/ee/operations/error_tracking.html) <br> [**Verify**](https://about.gitlab.com/stages-devops-lifecycle/verify/): [Browser Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html)  | [![Incremental rollout](../../images/youtube_social_icon_red-32x23.png) Application Release Automation & Delivery](https://youtu.be/ZAYBxLLcZrM) <br> [![Feature Flags](../../images/youtube_social_icon_red-32x23.png) Feature flags](https://youtu.be/_BZDM8LgGzg) |
| 6. Governance, Risk Management, and Compliance | -- | -- | -- |
| 7. Incident Management | --  | -- | -- |
| 8. Extensibility | Dozens of [project integrations](https://docs.gitlab.com/ee/user/project/integrations/overview.html), Webhooks, and an [open core](https://about.gitlab.com/company/stewardship/#business-model) model in which everyone can [contribute directly](https://about.gitlab.com/company/stewardship/#contributing-an-existing-feature-to-open-source-it) to the GitLab project.   | [**Manage stage:**](/stages-devops-lifecycle/manage/) [Integrations](https://docs.gitlab.com/ee/user/project/integrations/overview.html) | -- |


## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **A Single Application for the Entire DevOps Lifecycle** | A single application eliminates complex integrations, data chokepoints, and toolchain maintenance, resulting in greater productivity. | - |
| **End-to-End Visibility** | GitLab's common data model enables enables end-to-end visibility and traceability throughout the DevOps lifecycle, correlating and aggregating data automatically. | - |
| **Your software, deployed your way** | GitLab is infrastructure agnostic (supporting GCP, AWS, Azure, OpenShift, VMware, On Prem, Bare Metal, and more), offering a consistent workflow experience - irrespective of the environment. | - |


## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Customer Facing Slides
* [Simplify DevOps - Single Application Message Customer Presentation](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit#slide=id.g639141d4c5_0_15)

### Discovery Questions

The suggested discovery questions below are meant to help you uncover opportunities when speaking with prospects or customers who are not currently using GitLab or are only using GitLab as a point solution, and not experiencing the full value of an end-to-end platform.

Don’t try to use them all—just those most relevant to your customer that will help you identify the value they need to see. Please contribute additional questions!

#### Current state: Where’s the pain?

1. **How far along are you in your DevOps journey?** This will scope their interest. If the organization is new to DevOps, GitLab can be a guide toward DevOps best practices and help them avoid common pitfalls. If the company is well along their jounrey already, they likely have very specific challenges that GitLab can help overcome through the benefits of a single, end-to-end platform.
2. **What / how many DevOps tools are you using today?** Similarly, this will help scope the situation. A large number of tools will almost always equal integration complexity and sub-optimal communication among tools and teams. A very small number of tools could indicate a lack of autmation or an acceptance that there are just some things they cannot do (see below).
3. **How do you involve business stakeholders in your DevOps process?** This will reveal the coordination and alignment of technology / development with business. In many cases, it will be nonexistent or strained, and the single source of truth of a DevOps Platform can help. 
4. **How important are audits and compliance?** Audits are a well-understood risk with a well-documented cost, and they are one of the easiest problems for a DevOps Platform to solve. The presence of a strong audit/compliance culture can often indicate a desire for business stakeholders to gain visibility into DevOps / software production practices—something a DevOps Platform can enable.
5. **Can you predict your tool costs 2-3 years out?** A true platform will scale predictably. 

#### Future state: Where would you like to be?

1. **In an ideal world, what would collaboration look like in your organization?** In addition to spelling out success criteria, this can help identify companies who may not be thinking big enough because they aren't aware that a better option exists.
2. **Are there things you'd like to do with DevOps but complexity is holding you back?** This is a more explicit version of the question above, generally suitable for companies that are further along their DevOps joourneys.
3. **What if your security teams, business stakeholders, and others had access to all the information they needed on-demand?** This paints a very clear picture of both the benefits of collabopration and the extent to which a DevOps Platform can broaden engagement beyond developers and operations. It can reveal a desire to get more people included in the conversation.
4. **What if you could track all your discussions, the code changes that resulted from them, the performance and security impact of those changes, and all the collaboration around them--in one place, automatically?** This is similar to the question above, but more appropriate for a compliance-minded or frequently audited organization.
5. **What would you do if you could reclaim the time you spend integrating and maintaining your DevOps toolchain?** This provides the customer an opportunity to discuss what true success would look like if given the opportunity to rise above task work. This is the kind of transformational success a DevOps Platform can enable by freeing resources to work on more business-productive value.


## Competitive Comparison
Amongst the many competitors in the DevOps space, [Azure DevOps](/devops-tools/azure-devops-vs-gitlab.html) provides the most comprehensive end-to-end DevOps capabilities, with [GitHub](/devops-tools/github-vs-gitlab.html) continuing to build toward a similar vision.

### Industry Analyst Relations (IAR) Plan
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](/handbook/marketing/strategic-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-industry-analyst-interactions).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1124037301).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Proof Points - customers

### Quotes and reviews
<List of customer quotes/reviews from public sites>

### Case Studies
* [BI Worldwide](/customers/bi_worldwide/) removed technology barriers to focus on building microservices.
> One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role.
>
> Adam Dehnel, Product Architect, BI Worldwide

* [Glympse](/customers/glympse/) consolidated ~20 tools consolidated into GitLab and remediated security issues faster than any other company in their Security Auditor's experience
>Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts.
>
>Zaq Wiedmann, Lead Software Engineer, Glympse

* [Goldman Sachs](/customersman-sachs/) improves from bi-monthly builds to over a thousand per day
> GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!
>
> Andrew Knight, Managing Director, Goldman Sachs

### References to help you close
<Link to SFDC list of use case specific references>

## Key Value (at tiers)

### Premium
Generally, the more of the GitLab application in use, the greater the value of the platform story, because there are more people in more roles contributing and accessing more data in one place. In addition to the benefits listed on the [Why Premium page](https://about.gitlab.com/pricing/premium/), there are a number of standout benefits in Premium vs. Free. 

* **Substantially greater visibility** Advanced Search, Roadmaps, Contribution Analytics, burnup and burndown charts, and other visualizations make it far easier to achieve end-to-end visibility for a variety of needs.
* **Broader persona support** Features such as Epics, scoped labels, issue weights, and multiple issue assignees make Premium much more attractive as an enterprise-ready planning system, which, in turn, activates new personas such as Product Owners and Program Managers. Likewise, Auditor users and Audit Events extend the system to auditors.

### Ultimate
Ultimate supports the broadest possible range of personas, extending to Portfolio Managers with multi-level epics, as well as first-class support for Security and Compliance personas with role-specific dashboards and a host of other features. Ultimate also offers free Guest User accounts, which can invite other stakeholders to participate in discussions at no cost.


## Resources
### Presentations
* [Simplify DevOps - Single Application Message Customer Presentation](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit#slide=id.g639141d4c5_0_15)

### DevOps Platform Videos
* [Deliver more value with fewer headaches using an end-to-end DevOps platform](https://youtu.be/wChaqniv3HI)
* [Benefits of a single application](https://www.youtube.com/watch?v=MNxkyLrA5Aw)
* [GitLab in 3 minutes](https://www.youtube.com/watch?v=Jve98tlZ394)
* [Auto DevOps Click Through Demo Video](https://youtu.be/V_6bR0Kjju8?t=315)

### Integrations Demo Videos
* [Jira & Jenkins Integration Video](https://www.youtube.com/embed/Jn-_fyra7xQ)
* [How to set up the Jira Integration](https://www.youtube.com/watch?v=p56zrZtrhQE)
* [GitHub Integration Video](https://www.youtube.com/embed/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [All Marketing Click Through Demos](/handbook/marketing/strategic-marketing/demo/#click-throughs)
* [All Marketing Live Demos](/handbook/marketing/strategic-marketing/demo/#live-instructions)

## Buyer's Journey
Inventory of key pages in the buyer's Journey

| **Awareness** <br> learning about the problem  |  **Consideration** <br> looking for solution ideas  |  **Decision** <br> is this the right solution|
| ------ | -------- |-------- |
| [DevOps platform topic page](/topics/devops-platform/)  | [DevOps platform solution page](/solutions/devops-platform/) | [proof points]() |
| [landing pages?]() | ?comparisons?  | [comparisons]() |
| -etc?            |   |  - [product page x]() <br>  - [product page y]() <br>  - [product page z]() |
