---
layout: handbook-page-toc
title: "IR.2.01 - External Communication of Incidents Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IR.2.01 - External Communication of Incidents

## Control Statement

GitLab defines external communication requirements for incidents, including:

* Information about external party dependencies.
* Criteria for notification to external parties as required by GitLab policy in the event of a security breach.
* Contact information for authorities (e.g., law enforcement, regulatory bodies, etc.).
* Provisions for updating and communicating external communication requirement changes.

## Context

This control demonstrates that we have documented how we will communicate externally in the event of an incident.  This helps the company by making sure we will contact the necessary external parties.

## Scope

This control applies to the external communication of security or infrastructure incidents.

## Ownership

* Control Owner: `Security Operations`
* Process owner(s):
    * Security Operations: `25%`
    * Security Communications: `25%`
    * Infrastructure Operations: `25%`
    * Infrastructure Communications: `25%`

## Guidance

This control ensures GitLab's incident response communications plan has and maintains the essential components of external incident communication.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [External Communication of Incidents control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/842).

Examples of evidence an auditor might request to satisfy this control:

* Provide GitLab's incident external communication plan
* Provide samples showing the plan is followed for Infrastructure incidents
* Provide samples showing the plan is followed for Security incidents

### Policy Reference

* [Security Incident Communications Plan](/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html)
* [Infrastructure Incident Communication Plan](/handbook/engineering/infrastructure/incident-management/#communication)

## Framework Mapping

* ISO
  * A.6.1.3
* PCI
  * 12.10.1
