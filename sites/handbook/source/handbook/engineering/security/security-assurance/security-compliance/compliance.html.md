---
layout: handbook-page-toc
title: "Security Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Security Compliance Mission

It is the goal of the GitLab Security Compliance team to:

1. Enable GitLab sales by providing customers information and assurance about our information security program and remove security as a barrier to adoption by our customers.
1. Enable security to scale through the definition of security controls and determining the boundaries and applicability of the information security management system to establish its scope.
1. Work across industries and verticals to support GitLab customers in their own compliance journey.
1. Identify and mitigate GitLab information security risk through continuous control monitoring and automation.

## Security Compliance Core Competencies
A member of the [Security Assurance](/handbook/engineering/security/security-assurance/security-assurance.html) organization, these are the primary functions of the Security Compliance team:

1. [Governance](/handbook/engineering/security/security-assurance/security-compliance/Governance/)
   * GCF Control Maintenance
   * Security Compliance Handbook Pages
   * Security Policies and Standards
   * Security Compliance Training
   * Regulatory and Compliance Landscape Monitoring
   * Security Compliance Metrics
   * GRC Application Administration
1. [Internal Compliance Audit](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html)
   * [GCF Continuous Control Monitoring](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
   * [GitLab IT General Controls ITGC](/handbook/engineering/security/security-assurance/security-compliance/ITGC/)
1. [Security Certifications](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)
   * External Audits (e.g. SOC, FedRAMP, ISO, etc.)
   * Readiness Planning
   * Gap Assessments
1. [Observation and Remediation](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)
   * Control test findings  
   * External audit findings  
   * Gap analysis findings  
   * Customer assessments findings  
   * BitSight scanners findings  

## Security Compliance Work Inputs

1. GCF Continuous Control Testing
   * Controls are tested based on Security Certification requirements, GitLab Internal Audit team needs, and security risk.
1. External Audit requirements
   * When an external security audit is kicked off, work is performed as required within that audit.
1. Goveranance requirements
   * Work supporting the Security Compliance governance core competency is based on industry best practices, security certification requirements, and GitLab business need.
1. Customer Support
   * The Security Compliance team is engaged as subject matter experts to support specific security compliance customer requests. 
   * The Security Compliance team triages findings produced by external scanning services when responses are required according to the GitLab Risk and Field Security team.
1. Ad-hoc work streams
   * If you have a request for the GitLab Security Compliance team please [open an ad-hoc issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/new?issuable_template=ad_hoc_work.md) and we will review and prioritize that work weekly.

## Security Compliance Work Outputs
1. Governance documentation
   * The Security Compliance team manages security policies and standards.
   * The Security Compliance team evaluates GitLab handbook documentation through the course of continuous control testing and proposes updates as required.
1. Security Certifications (e.g. [SOC](/handbook/engineering/security/security-assurance/security-compliance/certifications.html), FedRAMP, ISO, etc.)
1. Remediation documentation
   * Control Test [Observations](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)
   * External Audit [Observations](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)
   * Gap Analysis [Observations](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)
1. Metrics/Reporting
   * The Security Compliance team provides the data relating to the health of the above [core competencies](#security-compliance-core-competencies).

## GitLab's Control Framework (GCF)

GitLab uses a common control framework that maps to a variety of industry compliance requirements and best practices. For information about how we developed this framework and a list of all of our security controls, please see the [security controls handbook page](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html).

## Ownership/DRI's

### Security Compliance Program DRI's
1. Internal Compliance Audit (GCF) - [Jeff Burrows](https://gitlab.com/jburrows001)
1. SOC/ISO - [Liz Coleman](https://gitlab.com/lcoleman)
1. FedRAMP/Phishing - [Nik Sarosy](https://gitlab.com/nsarosy)
1. GitLab IT General Controls (ITGC) - [Byron Boots](https://gitlab.com/byronboots)
1. Governance - [Rupal Shah](https://gitlab.com/rcshah)
1. Observations/Remediation - [Byron Boots](https://gitlab.com/byronboots) & [Jeff Burrows](https://gitlab.com/jburrows001)

### GitLab system DRI's
The Security Compliance team uses an application-based ownership model for control testing. The information below represents the current ownership for systems that have already been tested or scheduled to being testing. This list will expand as our continuous control testing expands to include new systems.

1. GitLab.com - [Liz Coleman](https://gitlab.com/lcoleman)
1. customers.gitlab.com - [Byron Boots](https://gitlab.com/byronboots)
1. Customers.GitLab - [Byron Boots](https://gitlab.com/byronboots)
1. Snowflake - [Nik Sarosy](https://gitlab.com/nsarosy)
1. Sisense - [Nik Sarosy](https://gitlab.com/nsarosy)
1. CaptivateIQ - [Rupal Shah](https://gitlab.com/rcshah)
1. gitlab.com database - [Nik Sarosy](https://gitlab.com/nsarosy)
1. gitlab.com database console - [Nik Sarosy](https://gitlab.com/nsarosy)
1. gitlab.com rails console (ssh) - [Nik Sarosy](https://gitlab.com/nsarosy)
1. Google Cloud Platform - [Liz Coleman](https://gitlab.com/lcoleman)
1. license.gitlab.com - [Byron Boots](https://gitlab.com/byronboots)
1. Okta - [Rupal Shah](https://gitlab.com/rcshah)
1. ADP - [Byron Boots](https://gitlab.com/byronboots)
1. Bastion Host - [Nik Sarosy](https://gitlab.com/nsarosy)
1. Chef Server (Ansible) - [Nik Sarosy](https://gitlab.com/nsarosy)
1. gitlab.com implement/deploy - [Liz Coleman](https://gitlab.com/lcoleman)
1. gitlab.com source code - [Liz Coleman](https://gitlab.com/lcoleman)
1. Workiva - [Rupal Shah](https://gitlab.com/rcshah)
1. Zuora - [Byron Boots](https://gitlab.com/byronboots)
1. NetSuite - [Rupal Shah](https://gitlab.com/rcshah)
1. Adaptive Planning - [Rupal Shah](https://gitlab.com/rcshah)
1. Avalara - [Byron Boots](https://gitlab.com/byronboots) 
1. BambooHR - [Nik Sarosy](https://gitlab.com/nsarosy)
1. Expensify - [Rupal Shah](https://gitlab.com/rcshah)
1. Greenhouse - [Rupal Shah](https://gitlab.com/rcshah)
1. PagerDuty - [Rupal Shah](https://gitlab.com/rcshah)
1. SalesForce - [Liz Coleman](https://gitlab.com/lcoleman)
1. Tenable.IO - [Byron Boots](https://gitlab.com/byronboots)
1. Tipalti (Coupa) - [Nik Sarosy](https://gitlab.com/nsarosy)
1. Alyce - [Liz Coleman](https://gitlab.com/lcoleman)
1. American Express - [Liz Coleman](https://gitlab.com/lcoleman)
1. Carta - [Byron Boots](https://gitlab.com/byronboots)
1. FloQast - [Rupal Shah](https://gitlab.com/rcshah)
1. Mavenlink - [Byron Boots](https://gitlab.com/byronboots)

## Contact the Compliance Team

* Email
   * `security-compliance@gitlab.com`
* Tag us in GitLab
   * `@gitlab-com/gl-security/security-assurance/sec-compliance`
* Slack
   * Feel free to tag is with `@sec-compliance-team`
   * The #sec-assurance slack channel is the best place for questions relating to our team (please add the above tag)
* [GitLab compliance project](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance)
* Interested in joining our team? Check out more [here](https://about.gitlab.com/job-families/engineering/security-compliance/)!
