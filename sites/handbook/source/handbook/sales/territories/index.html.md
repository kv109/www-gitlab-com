---
layout: handbook-page-toc
title: "Sales Territories"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Process to Request Update

### Territory Ownership (Sales)

1. Create an issue in the **Sales Operations** project - utilizing the [Territory Change Request template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=Territory_Change_Request)
1. Follow the directions within the template & provide all the requested details
    - If **Individual Contributor** is requesting the change, ADD your manager to the `/assign` command
    - If **Manager** is requesting change, submit issue & it will auto-assign to Sales Ops
1. `Sales Operations` to update SFDC
1. Change made on Territory Management document by `Sales Operations` **after** change in system has been made.
1. `Sales Operations` to update LeanData download updated csv.
1. Territory Management updates will be uploaded to LeanData by `Sales Operations` **after** change in system has been made.

### Territory Ownership (ISR)

1. Create an issue in the **Sales Operations** project - utilizing the [ISR Territory Change template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=ISRTerritoryChange)
1. Follow the directions within the template & provide all the requested details
1. `Sales Operations`and `Sales Systems` to update SFDC
1. `Sales Operations`to update the [ISR Territories Mapping File](https://docs.google.com/spreadsheets/d/1dzA_t3Nfa_C-qd-QJ41BkMy6I_A76FTTmXTXb57qZuI/edit#gid=0) 
1. To request ISR ownership exceptions for individual opportunities, please chatter your ISR manager on the opportunity

#### Updating these tables without updating Operations will not be reflected in our various systems causing all reports and routing to be incorrect!

{:.no_toc}

**Questions?** Ask in `#sales` slack channel pinging `@sales-ops`.

## Region/Vertical

{:.no_toc}

- **VP Commercial Sales** ([Mid-Market](#mid-market) & [Small Business](/handbook/sales/territories/territory-deprecate/#small-business-segment)): Ryan O'Nell
- <b>[APAC](#apac)</b>: Anthony McMahon, Regional Director
- <b>[Europe, Middle East and Africa](#emea)</b>: Jon Burghart, Regional Director
- <b>[North America - US East](/handbook/sales/territories/territory-deprecate/#us-east)</b>: TBD, Regional Director
- <b>[North America - US West](/handbook/sales/territories/territory-deprecate/#us-west)</b>: Haydn Mackay, Regional Director
- <b>[Public Sector](#public-sector)</b>: Jim Riley (Interim), Director of Federal Sales

## Territories

For the detailed breakdown of how the territories are mapped please reference the [Territories Mapping File - SSoT Master tab](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit?usp=sharing)

### Large

#### AMER

##### Area Sales Manager

{:.no_toc}

- **NA East - Named Accounts**: TBD
- **NA East - Southeast**: Tom Plumadore
- **NA East - Northeast**: Sheila Walsh
- **NA East - Central**: Adam Olson
- **NA West - Rockies/SoCal**: TBD
- **NA West - Bay Area**: Alan Cooke
- **NA West - PNW/MidWest**: Timm Ideker

| Sub-Region | Area | **Territory Name** | Sales |Inside Sales Rep |
| ---------- | ---- | -------------- | ----- | ----- |
|	NA East	|	East	|	Large-AMER-East-Named-1	|	Mark Bell	| David Fisher
|	NA East	|	East	|	Large-AMER-East-Named-2	|	Jordan Goodwin	| David Fisher
|	NA East	|	East	|	Large-AMER-East-Named-3	|	Chip Digirolamo	| David Fisher
|	NA East	|	East	|	Large-AMER-East-Named-4	|	Josh Rector | David Fisher
|	NA East	|	East	|	Large-AMER-East-Named-5	|	Sean Billow	| David Fisher
|	NA East	|	East	|	Large-AMER-East-Named-6	|	Scott Hall	| David Fisher
|	NA West	|	Southwest	|	Large-AMER-West-1	|	Haydn Mackay	| Matthew Beadle
|	NA West	|	Southwest	|	Large-AMER-West-2	|	Alan Cooke	| Matthew Beadle
|	NA West	|	NorthWest	|	Large AMER-PNW/MW-1	|	Adi Wolff	| Matthew Beadle
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-2	|	Joe Drumtra	| Matthew Beadle
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-3	|	Chris Mayer	| Matthew Beadle
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-4	|	Philip Wieczorek	| Matthew Beadle
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-5	|	Lydia Pollitt	| Matthew Beadle
|	NA West	|	NorCal	|	Large-AMER-Bay Area-1	|	Alyssa Belardi	| Matthew Beadle
|	NA West	|	NorCal	|	Large-AMER-Bay Area-2	|	Jim McMahon	| Matthew Beadle
|	NA West	|	NorCal	|	Large-AMER-Bay Area 3	|	Nico Ochoa	| Matthew Beadle
|	NA West	|	NorCal	|	Large-AMER-Bay Area 4	|	Joe Miklos	| Matthew Beadle
|	NA West	|	NorCal	|	Large-AMER-Bay Area 5	|	Michael Nevolo	| Matthew Beadle
|	NA West	|	NorCal	|	Large-AMER-Bay Area 6	|	Michael Scott	| Matthew Beadle
|	NA East	|	Northeast	|	Large-AMER-Central-1	|	Tim Kuper	| David Fisher
|	NA East	|	Central	|	Large-AMER-Central-2	|	Matt Petrovick	| David Fisher
|	NA East	|	Central	|	Large-AMER-Central-3	|	Brandon Greenwell	| David Fisher
|	NA East	|	Central	|	Large-AMER-Central-4	|	Ruben Govender	| David Fisher 
|	[LATAM](/handbook/sales/territories/latam/)		|	LATAM North	|	Large-AMER-LATAM-1	|	Carlos Dominguez	| David Fisher
|	[LATAM](/handbook/sales/territories/latam/)		|	LATAM South	|	Large-AMER-LATAM-2	|	Jim Torres	| David Fisher
|	NA West	|	Midwest	|	Large-AMER-Midwest-1	|	Timmothy Ideker	| Matthew Beadle
|	NA East	|	Northeast	|	Large-AMER-Northeast-1	|	TBH	| David Fisher
|	NA East	|	Northeast	|	Large-AMER-Northeast-2	|	Paul Duffy	| David Fisher
|	NA East	|	Northeast	|	Large-AMER-Northeast-3	|	Peter McCracken	| David Fisher
|	NA East	|	Northeast	|	Large-AMER-Northeast-4	|	Tony Scafidi	| David Fisher
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-1	|	Rick Walker	| Matthew Beadle
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-2	|	Chris Cornacchia	| Matthew Beadle
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-3	|	Brad Downey	| Matthew Beadle
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-4	|	Steve Clark	| Matthew Beadle
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-5	|	Robert Hyry	| Matthew Beadle
|	NA East	|	Southeast	|	Large-AMER-Southeast-1	|	Chris Graham	| David Fisher
|	NA East	|	Southeast	|	Large-AMER-Southeast-2	|	Katherine Evans	| David Fisher
|	NA East	|	Southeast	|	Large-AMER-Southeast-3	|	Jim Bernstein	| David Fisher
|	NA East	|	Southeast	|	Large-AMER-Southeast-4	|	David Wells	| David Fisher
|	NA East	|	Southeast	|	Large-AMER-Southeast-5	|	Larry Biegel	| David Fisher



#### Public Sector

| Sub-Region | **Territory Name** | Strategic Account Leader | Inside Sales Rep | SDR
| ---------- | -------------- | ------------------------ | ---------------- | ---------------- |
| Public Sector | **Federal - Civilian-2** | Susannah Reed | Christine Saah | Evan Mathis |
| Public Sector | **Federal - Civilian-3** | Luis Vazquez | Bill Duncan | Evan Mathis |
| Public Sector | **Federal - Civilian-5** | Joel Beck | Nathan Houston | Evan Mathis |
| Public Sector | **Federal - Civilian-6** | Matt Kreuch | Christine Saah | Evan Mathis |
| Public Sector | **Federal - Civilian-7** | Tyler Kensky | Christine Saah | Evan Mathis |
| Public Sector | **State and Local (SLED East)** | Dan Samson | Alexis Shaw | Evan Mathis |
| Public Sector | **State and Local (SLED Central)** | Matt Stamper | Victor Brew | Evan Mathis |
| Public Sector | **State and Local (SLED South)** | Mark Williams | Alexis Shaw | Evan Mathis |
| Public Sector | **Federal - DoD-Air Force-1** | Matt Jamison | Craig Pepper | Josh Downey |
| Public Sector | **Federal - DoD-Air Force-2** | TBH | Craig Pepper | Josh Downey |
| Public Sector | **Federal - DoD-Air Force-3** | Stan Brower | Craig Pepper | Josh Downey |
| Public Sector | **Federal - DoD-Navy-1** | TBH | Patrick Gerhold | Josh Downey |
| Public Sector | **Federal - DoD-Navy-2** | Chris Rennie | Patrick Gerhold | Josh Downey |
| Public Sector | **Federal - DoD-Army-1** | Ron Frazier | Patrick Gerhold | Josh Downey |
| Public Sector | **Federal - DoD-Army-2** | Allison Mueller | Patrick Gerhold | Josh Downey |
| Public Sector | **Federal - DoD-Agencies** | Scott McKee | Joe Fentor | Josh Downey |
| Public Sector | **Federal - NSG-1** | Marc Kriz | Joe Fenter | Josh Downey |
| Public Sector | **Federal - NSG-2** | Mike Sellers | Joe Fenter | Josh Downey |
| Public Sector | **Federal - NSG-3** | Ian Moore | Nate Houston | Evan Mathis |
| Public Sector | **Federal - NSG-4** | Russ Wilson | Bill Duncan | Evan Mathis |
| Public Sector | **Federal - NSG-5** | Garry Judy | Nate Houston | Evan Mathis |

#### APAC

| Sub-Region | Area | **Territory Name** | Sales | Inside Sales Rep
| ---------- | ---- | -------------- | ----- | 
|	Japan	|	Japan	|	Large-APAC-Japan-1	|	Tadashi Murakami	| Daphne Yam
|	Japan	|	Japan	|	Large-APAC-Japan-2	|	Eiji Morita	| Daphne Yam
|	Japan	|	Japan	|	Large-APAC-Japan-3	|	Yuki Murakami	| Daphne Yam
|	APAC	|	APAC	|	Large-APAC-Large-APAC-1	|	Danny Petronio	| Daphne Yam
|	ANZ	|	ANZ	|	Large-APAC-Large-APAC-2	|	Rob Hueston	| Daphne Yam
|	ANZ	|	ANZ	|	Large-APAC-Large-APAC-3	|	David Haines	| Daphne Yam
|	Asia SE	|	Southeast Asia	|	Large-APAC-Large-APAC-4	|	Hui Hui Cheong	| Daphne Yam
|	Asia Central	|	Asia Central	|	Large-APAC-Large-APAC-5	|	Rob Hueston	| Daphne Yam
|	Asia Central	|	Asia Central	|	Large-APAC-S Korea-1	|	Tae Ho Hyun	| Daphne Yam

#### EMEA

| Sub-Region | Area | **Territory Name** | Sales | Inside Sales Rep
| ---------- | ---- | -------------- | ----- | 
|	Europe Central	|	Europe Central	|	Large-EMEA-DACH-1	|	Rene Hoferichter	| Anthony Seguillon
|	Europe Central	|	Germany	|	Large-EMEA-DACH-2	|	Rene Hoferichter	| Anthony Seguillon
|	Europe Central	|	Germany	|	Large-EMEA-DACH-3	|	Christoph Stahl	| Anthony Seguillon
|	Europe Central	|	Europe Central	|	Large-EMEA-DACH-4	|	Timo Schuit	| Anthony Seguillon
|	Europe Central	|	Eastern Europe	|	Large-EMEA-Large-EMEA-1	|	Vadim Rusin	| Anthony Seguillon
|	MEA	|	MEA	|	Large-EMEA-Large-EMEA-2	|	Phillip Smith	| Anthony Seguillon
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-3	|	Anthony Seguillon (SAL)	| Anthony Seguillon
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-4	|	Anthony Seguillon (SAL)	| Anthony Seguillon
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-5	|	Vadim Rusin	| Anthony Seguillon
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-6	|	Hugh Christey	| Anthony Seguillon
|	Nordics	|	Nordics	|	Large-EMEA-UK/I-1	|	Annette Kristensen	| Anthony Seguillon
|	Nordics	|	Nordics	|	Large-EMEA-UK/I-2	|	Aslihan Kurt	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-3	|	Robbie Byrne	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-4	|	Nasser Mohunlol	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-5	|	Justin Haley	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-6	|	Nicholas Lomas	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-7	|	Simon Williams	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-8	|	Steve Challis	| Anthony Seguillon
|	UKI	|	UKI	|	Large-EMEA-UK/I-9	|	Peter Davies	| Anthony Seguillon


### Mid-Market

#### AMER

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | 
|	NA East	|	US East	|	MM-AMER-East-Named-1	|	Sharif Bennett	|
|	NA East	|	US East	|	MM-AMER-East-Named-2	|	Steve Xu	|
|	NA East	|	US East	|	MM-AMER-East-Named-3	|	Daniel Parry	|
|	NA East	|	US East	|	MM-AMER-West-Named-1	|	Christopher Chiappe	|
|	NA East	|	US East	|	MM-AMER-West-Named-2	|	Kyla Gradin	|
|	NA East	|	US East	|	MM-AMER-West-Named-3	|	Matthew Kobilka	|
|	NA East	|	US East	|	MM-AMER-EAST-CTL-1	|	Michael Miranda	|
|	NA East	|	US East	|	MM-AMER-EAST-CTL-2	|	Jenny Kline	|
|	[LATAM](/handbook/sales/territories/latam/)	|	US East	|	MM-AMER-EAST-LATAM	|	Romer Gonzalez	|
|	NA East	|	US East	|	MM-AMER-EAST-MidAtlantic	|	Jenny Kline	|
|	NA East	|	US East	|	MM-AMER-EAST-NE	|	Michael Miranda	|
|	NA East	|	US East	|	MM-AMER-EAST-SE	|	Jenny Kline	|
|	NA East	|	US East	|	MM-AMER-EAST-Southeast	|	Daniel Parry	|
|	NA West	|	US West	|	MM-AMER-WEST-Mtn	|	Laura Shand	|
|	NA West	|	US West	|	MM-AMER-WEST-NorCal	|	Brooke Williamson	|
|	NA West	|	US West	|	MM-AMER-WEST-NW	|	Brooke Williamson	|
|	NA West	|	US West	|	MM-AMER-WEST-SF	|	Laura Shand	|
|	NA West	|	US West	|	MM-AMER-WEST-SoCal	|	Douglas Robbin	|

#### APAC

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | 
| ANZ	|	ANZ	|	MM-APAC-ANZ	|	Ian Chiang	|
|	Asia Central	|	Asia Central	|	MM-APAC-Central Asia	|	Ishan Padgotra	|
|	Japan	|	Japan	|	MM-APAC-Japan	|	Ian Chiang	|
|	Asia Central	|	Pakistan	|	MM-APAC-Pakistan	|	Ishan Padgotra	|
|	Asia SE	|	Southeast Asia	|	MM-APAC-SE Asia	|	Ian Chiang	|
|	Asia South	|	Asia South	|	MM-APAC-South Asia	|	Ishan Padgotra	|

#### EMEA

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | 
|	EMEA	|	MEA	|	MM-EMEA-Named-1	|	Anthony Ogunbowale-Thomas	|
|	EMEA	|	MEA	|	MM-EMEA-Named-2	|	Israa Mahros	|
|	Africas	|	Africas	|	MM-EMEA-Africas	|	Daisy Miclat	|
|	Benelux	|	BE/LU	|	MM-EMEA-Benelux-BeLu	|	Hans Frederiks	|
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 10x-19x	|	Hans Frederiks	|
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 20x-29x	|	Hans Frederiks	|
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 30x-39x	|	Hans Frederiks	|
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 40x-99x	|	Hans Frederiks	|
|	Europe Central	|	AT	|	MM-EMEA-Central-AT	|	Conor Brady	|
|	Europe Central	|	CH	|	MM-EMEA-Central-CH	|	Conor Brady	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 0x	|	Chris Willis	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 1x	|	Chris Willis	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 2x	|	Conor Brady	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 3x	|	Chris Willis	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 4x	|	Hans Frederiks	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 5x	|	Conor Brady	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 6x	|	Chris Willis	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 7x	|	Chris Willis	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 8x	|	Hans Frederiks	|
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 9x	|	Hans Frederiks	|
|	Europe Central	|	LI	|	MM-EMEA-Central-LI	|	Conor Brady	|
|	Europe CEE	|	R	|	MM-EMEA-Eastern Europe	|	Chris Willis	|
|	France	|	FR	|	MM-EMEA-France-FR 0x-6x	|	Israa Mahros	|
|	France	|	FR	|	MM-EMEA-France-FR 7x	|	Conor Brady	|
|	France	|	FR	|	MM-EMEA-France-FR 8x-9x	|	Conor Brady	|
|	France	|	R	|	MM-EMEA-France-R	|	Conor Brady	|
|	MEA	|	AE	|	MM-EMEA-MEA-AE	|	Daisy Miclat	|
|	MEA	|	R	|	MM-EMEA-MEA-R	|	Daisy Miclat	|
|	MEA	|	SA	|	MM-EMEA-MEA-SA	|	Daisy Miclat	|
|	Nordics	|	DK	|	MM-EMEA-Nordics-DK	|	Hans Frederiks	|
|	Nordics	|	FI	|	MM-EMEA-Nordics-FI	|	Conor Brady	|
|	Nordics	|	NO	|	MM-EMEA-Nordics-NO	|	Hans Frederiks	|
|	Nordics	|	R	|	MM-EMEA-Nordics-R	|	Hans Frederiks	|
|	Nordics	|	SE	|	MM-EMEA-Nordics-SE	|	Conor Brady	|
|	Russia	|	RU	|	MM-EMEA-Russia	|	Daisy Miclat	|
|	Southern Europe	|	ES	|	MM-EMEA-Southern-ES	|	Hans Frederiks	|
|	Southern Europe	|	IL	|	MM-EMEA-Southern-IL	|	Conor Brady	|
|	Southern Europe	|	IT	|	MM-EMEA-Southern-IT	|	Hans Frederiks	|
|	Southern Europe	|	PT	|	MM-EMEA-Southern-PT	|	Hans Frederiks	|
|	Southern Europe	|	R	|	MM-EMEA-Southern-R	|	Hans Frederiks	|
|	UKI	|	GB	|	MM-EMEA-UKI-GB	|	Daisy Miclat	|
|	UKI	|	IE	|	MM-EMEA-UKI-Ireland	|	Hans Frederiks	|
|	UKI	|	GB	|	MM-EMEA-UKI-London E	|	Chris Willis	|
|	UKI	|	GB	|	MM-EMEA-UKI-London EC	|	Chris Willis	|
|	UKI	|	GB	|	MM-EMEA-UKI-London N	|	Chris Willis	|
|	UKI	|	GB	|	MM-EMEA-UKI-London NW	|	Chris Willis	|
|	UKI	|	GB	|	MM-EMEA-UKI-London SE	|	Conor Brady	|
|	UKI	|	GB	|	MM-EMEA-UKI-London SW	|	Conor Brady	|
|	UKI	|	GB	|	MM-EMEA-UKI-London W	|	Conor Brady	|
|	UKI	|	GB	|	MM-EMEA-UKI-London WC	|	Conor Brady	|
|	UKI	|	R	|	MM-EMEA-UKI-R	|	Daisy Miclat	|

### Mid-Market First Order

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | 
|	NA East	|	US East	|	MM-AMER-FO-East	|	Todd Lauver	|
|	NA West	|	US West	|	MM-AMER-FO-West	|	Rashad Bartholomew	|
|	Northern Europe	|	Northern Europe	|	MM-EMEA-FO-North	|	Lisa VdKooij	|
|	Southern Europe	|	Southern Europe	|	MM-EMEA-FO-South	|	Sophia Simunec	|

### SMB

#### AMER

| Sub-Region | Area | **Territory Name** | Sales |
| ---------- | ---- | -------------- | ----- | 
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-1	|	Jenny Chapman
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-2	|	Anthony Feldman
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-3	|	Matthew Walsh
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-4	|	Kaley Johnson
|	[LATAM](/handbook/sales/territories/latam/)	|	US East	|	SMB-AMER-EAST-LATAM	|	Romer Gonzalez
|	AMER	|	AMER	|	SMB-AMER-EAST-MidAtl	|	Jenny Chapman
|	AMER	|	AMER	|	SMB-AMER-EAST-NE	|	Matthew Walsh
|	AMER	|	AMER	|	SMB-AMER-EAST-NY	|	Anthony Feldman
|	AMER	|	AMER	|	SMB-AMER-EAST-SE	|	Kaley Johnson
|	AMER	|	AMER	|	SMB-AMER-WEST-MW	|	Adam Pestreich
|	AMER	|	AMER	|	SMB-AMER-WEST-NorCal |	James Altheide
|	AMER	|	AMER	|	SMB-AMER-WEST-NW	|	Marsja Jones
|	AMER	|	AMER	|	SMB-AMER-WEST-SF	|	Marsja Jones
|	AMER	|	AMER	|	SMB-AMER-WEST-SoCal	|	Carrie Nicholson
|	AMER	|	AMER	|	SMB-AMER-WEST-SW	|	Adam Pestreich

#### APAC

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- |
|	ANZ	|	ANZ	|	SMB-APAC-ANZ	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-JAPAN	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-SE Asia	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-Central Asia	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-Pakistan	|	Ishan Padgotra	|

#### EMEA

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | --- |
|	Europe Central	|	BeNeLux	|	SMB-EMEA-BeNeLux	|	Vilius Kavaliauskas
|	EMEA	|	EMEA	|	SMB-EMEA-DE-1	|	Gábor Zaparkanszky
|	EMEA	|	EMEA	|	SMB-EMEA-DE-2	|	Gábor Zaparkanszky
|	EMEA	|	EMEA	|	SMB-EMEA-DE-3	|	Rahim Abdullayev
|	Europe CEE	|	Eastern Europe	|	SMB-EMEA-Eastern Europe	|	Arina Voytenko
|	Southern Europe	|	EMEA	|	SMB-EMEA-FR-1	|	Wiam Aissaoui
|	Southern Europe	|	EMEA	|	SMB-EMEA-FR-2	|	Wiam Aissaoui
|	Southern Europe	|	Europe South	|	SMB-EMEA-Greece	|	Arina Voytenko
|	MEA	|	MEA	|	SMB-EMEA-MEA	|	Camilo Villanueva
|	Nordics	|	Nordics	|	SMB-EMEA-Nordics-1	|	Camilo Villanueva
|	Nordics	|	Nordics	|	SMB-EMEA-Nordics-2	|	Arina Voytenko
|	Europe Central	|	DACH	|	SMB-EMEA-Rest of DACH	|	Rahim Abdullayev
|	Southern Europe	|	Europe South	|	SMB-EMEA-Southern Europe	|	Camilo Villanueva
|	UKI	|	UKI	|	SMB-EMEA-UKI-1	|	Tim Guibert
|	UKI	|	UKI	|	SMB-EMEA-UKI-2	|	Tim Guibert

### SMB First Order

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | 
|	AMER	|	US West	|	SMB-AMER-West-FO	|	Todd Lauver	|
