---
layout: handbook-page-toc
title: "Data Team"
description: "GitLab Data Team Handbook our mission is to deliver results that matter with trusted and scalable data solutions."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## Welcome to the Data Team Handbook

* Our Mission is to **Deliver Results That Matter With Trusted and Scalable Data Solutions.**
* Read our [Direction](/handbook/business-technology/data-team/direction/) page to learn _what_ we are doing to improve data at GitLab.
* Our [Principles](/handbook/business-technology/data-team/principles/) inform how we accomplish our mission.

**Would you like to contribute? [Become a Data Champion](/handbook/business-technology/data-team/direction/data-champion/), [recommend an improvement](https://gitlab.com/gitlab-data/analytics/-/issues), [visit #data](https://gitlab.slack.com/messages/data/), [watch a Data Team video](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI). We want to hear from you!**
{: .alert .alert-success}

## Data Teams at GitLab
 
GitLab has three primary distinct teams that use data to drive insights and business decisions.  These teams are complementary to one another and are focused on specific areas to drive a deeper understanding of trends in the business.  The three areas are the **Data Team**, **Business Insights**, and **analysts located in Sales, Marketing, Product, or Engineering**.
 
Here are how the team members divide their work and work with each other to ensure the best results:
 
* The **Data Team** reports into Business Technology and is responsible for analytics strategy, building business-wide data models, providing self-service data capabilities, maintaining the data warehouse and data accuracy.  The Data Team is responsible for data that is defined and accessed on a regular basis by GitLab team members. The Data Team builds data infrastructure to power approximately 80% of the data that is accessed on a regular basis.
 
* **Analytics and Business Insights** reports into FP&A and provides a broad view of performance across the business. Business Insights primarily work on company-wide strategic analysis and data that spans various departments – sales, marketing, product, engineering, people, and finance. To the extent this analysis leads to insights that should be produced on a regular basis, Business Insights will work with the Data team to create a self-service dashboard.  The distinction between Business Insights and the Data Team is deep dive analysis that currently doesn’t have a dashboard to drive deeper business insights with data that resides in multiple areas.

* **Sales, Marketing, Product, and Engineering Analysts**, or “Function Analysts” reside in their respective functions. These analysts perform specific analysis for data that is within the function. These analysts will build dashboards based on the centralized trusted data model for regular analysis, and will also build function-specific/ad-hoc data models to solve pressing needs. Function analysts work closely with both the Business Insights and Data Team. Function analyst team members provide requirements for new trusted data dashboards, validate metrics, and help drive prioritization of work asked of the Data Team. When data gaps are found in our business processes and source systems, the team members will provide requirements to product management, sales ops and marketing ops to ensure the source systems capture the correct data.


## <i class="fas fa-info-circle" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>How We Can Help You

### Self-Service Data

We are actively building _[Self-Service Analytics Workflows](/handbook/business-technology/data-team/direction/self-service/)_ across our Go-To-Marketing and Product Spaces, enabling you to do more with data by yourself. Examples include [Product Geolocation Analysis](/handbook/business-technology/data-team/data-catalog/product-geolocation) and [Customer Segmentation Analysis](/handbook/business-technology/data-team/data-catalog/customer-segmentation), with more coming soon.

- [Review the KPI Index](/handbook/ceo/kpis/#gitlab-kpis) to see how GitLab is performing or to [Develop a new KPI](/handbook/business-technology/data-team/kpi-index/#kpi-development).
- [Browse the Data Catalog](/handbook/business-technology/data-team/data-catalog/index.html#data-catalog) to find ready-to-use Self-Service Analytics Workflows, Data Definitions, and Dashboards.
- [Learn about Sisense](/handbook/business-technology/data-team/platform/periscope/) to access existing Dashboards or build your own Dashboard.
- [Review our Data Sources](/handbook/business-technology/data-team/platform/#extract-and-load) to see data we have available for analysis.
- [Conduct your own SQL Analysis](/handbook/business-technology/data-team/platform/#warehouse-access) with our Snowflake Data Warehouse.
- [SheetLoad: Load spreadsheets and CSVs into our data warehouse](/handbook/business-technology/data-team/platform/#using-sheetload) that you can connect to Sisense Dashboards.

### Data Programs

Explore the programs we've created to help you in your analytics journey.

- [Data Champion](/handbook/business-technology/data-team/direction/data-champion/)
- [Data for Product Managers](/handbook/business-technology/data-team/programs/data-for-product-managers/)
- [Data for Finance](/handbook/business-technology/data-team/programs/data-for-finance/)
- [Data Team on call for Special Events](/handbook/business-technology/data-team/data-service/#special-event-on-call)

### <i class="fas fa-bullhorn fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>Connect With Us

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://gitlab.slack.com/messages/data/" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">#Data Slack Channel</a>
  <a href="https://gitlab.com/gitlab-data/analytics/-/issues" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Issue tracker</a>
  <a href="https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">GitLab Unfiltered Data Team Playlist</a>
  <a href="https://www.worldtimebuddy.com/?pl=1&lid=2950159,6173331,4487042,4644585&h=2950159" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">What time is it for folks on the data team?</a>
</div>
<br>

## How We Work

### Responsibilities

The Data Team is a Sub-department of the [Business Technology](/handbook/business-technology) Department and provides a Data & Analytics platform, programs, and services to the entire company.

- Define and publish a Data Strategy to help maximize the value of GitLab's Data Assets
- Build and maintain the company's central Enterprise Data Warehouse to support Reporting, Analysis, [Dimensional Modeling](https://www.kimballgroup.com/data-warehouse-business-intelligence-resources/kimball-techniques/dimensional-modeling-techniques/), and Data Development for all GitLab teams
- Integrate new data sources to enable analysis of subject areas, activities, and processes
- Build and maintain an Enterprise Dimensional Model to enable [Single Source of Truth](https://en.wikipedia.org/wiki/Single_source_of_truth) results
- Develop Data Management features such as master data, reference data, data quality, data catalog, and data publishing
- Support the company's governance, risk, and compliance programs as they relate to Data & Analytics systems
- Provide Self-Service Data capabilities to help everyone leverage data and analytics
- Help to define and champion Data Quality practices and programs for GitLab data systems
- Provide customizable Data Services, including Data Visualization, Data Modeling, Data Quality, and Data Integration
- Broadcast regular updates about data deliverables, ongoing initiatives, and upcoming plans

### Slack

The Data Team primarily uses these channels on Slack:

- [#data](https://gitlab.slack.com/messages/data/) is the primary channel for all of GitLab's data and analysis conversations. This is where folks from other teams can link to their issues, ask for help, direction, and get general feedback from members of the Data Team.
- [#data-daily](https://gitlab.slack.com/messages/data-daily/) is where the Data Team tracks day-to-day productivity, blockers, and fun. Powered by [Geekbot](https://geekbot.com/), it's our asynchronous version of a daily stand-up, and helps keep everyone on the Data Team aligned and informed.
- [#data-lounge](https://gitlab.slack.com/messages/data-lounge/) is for links to interesting articles, podcasts, blog posts, etc. A good space for casual data conversations that don't necessarily relate to GitLab. Also used for intrateam discussion for the Data Team.
- [#data-engineering](https://gitlab.slack.com/messages/data-engineering/) is where the GitLab Data Engineering team collaborates.
- [#business-technology](https://gitlab.slack.com/messages/business-technology/) is where the Data Team coordinates with Business Technology in order to support scaling, and where all Business Technology-related conversations occur.
- [#analytics-pipelines](https://gitlab.slack.com/messages/analytics-pipelines/) is where slack logs for the ELT pipelines are output and is for data engineers to maintain.  The DRI for tracking and triaging issues from this channel is shown [here](/handbook/business-technology/data-team/platform/infrastructure/#data-infrastructure-monitoring-schedule)
- [#dbt-runs](https://gitlab.slack.com/messages/dbt-runs/), like #analytics-pipelines, is where slack logs for [dbt](https://www.getdbt.com/) runs are output. The Data Team tracks these logs and triages accordingly.
- [#data-triage](https://gitlab.slack.com/messages/data-triage/) is an activity feed of opened and closed issues and MR in the data team project.
- [#data-decisive-data-projects](https://gitlab.slack.com/messages/data-decisive-data-projects/) is where we discuss major data development projects, including our trusted data enterprise dimensional model.

You can also tag subsets of the Data Team using:

- @datateam - this notifies the entire Data Team
- @data-engineers - this notifies just the Data Engineers
- @data-analysts - this notifies just the Data Analysts

Except for rare cases, conversations with folks from other teams should take place in #data, and possibly the fusion team channels when appropriate.  Posts to other channels that go against this guidance should be responded to with a redirection to the #data channel, and a link to this handbook section to make it clear what the different channels are for.

### GitLab Groups and Projects

The Data Team primarily uses these groups and projects on GitLab:

- [GitLab Data](https://gitlab.com/gitlab-data) is the main group for the GitLab Data Team.
- [GitLab Data Team](https://gitlab.com/gitlab-data/analytics) is the primary project for the GitLab Data Team.

You can tag the Data Team in GitLab using:

- @gitlab-data  - this notifies the entire Data Team
- @gitlab-data/engineers  - this notifies just the Data Engineers
- @gitlab-data/analysts - this notifies just the Data Analysts

### Team, Operations, and Technical Guides

|  **TECH GUIDES** | **INFRASTRUCTURE** | **DATA TEAM** |
| :--------------- | :----------------- | :-------------- |
| [SQL Style Guide](/handbook/business-technology/data-team/platform/sql-style-guide/) | [High Level Diagram](/handbook/business-technology/data-team/platform/#our-data-stack/) | [How We Work](/handbook/business-technology/data-team/how-we-work/) 
| [dbt Guide](/handbook/business-technology/data-team/platform/dbt-guide/) | [System Data Flows](/handbook/business-technology/data-team/platform/infrastructure/#system-diagram/) | [Team Organization](/handbook/business-technology/data-team/organization) 
| [Python Guide](/handbook/business-technology/data-team/platform/python-guide/) | [Data Sources](/handbook/business-technology/data-team/platform/#extract-and-load/)| [Calendar](/handbook/business-technology/data-team/how-we-work/calendar/) 
| [Airflow & Kubernetes](/handbook/business-technology/data-team/platform/infrastructure/#common-airflow-and-kubernetes-tasks/) | [Snowplow](/handbook/business-technology/data-team/platform/snowplow/)  | [Triage](/handbook/business-technology/data-team/how-we-work/duties/#data-triage) 
| [Docker](/handbook/business-technology/data-team/platform/infrastructure/#docker) | [Permifrost](/handbook/business-technology/data-team/platform/permifrost) | [Merge Requests](/handbook/business-technology/data-team/how-we-work/duties/#merge-request-roles-and-responsibilities/) 
| [Data CI Jobs](/handbook/business-technology/data-team/platform/ci-jobs) | [DataSiren](/handbook/business-technology/data-team/platform/#datasiren/) | [Planning Drumbeat](/handbook/business-technology/data-team/planning/) 
| [SiSense Style Guide](/handbook/business-technology/data-team/platform/sisense-style-guide/) | [Trusted Data](/handbook/business-technology/data-team/platform/#tdf)
| [Learning Library](/handbook/business-technology/data-team/learning-library/)

### GitLab Unfiltered

- [Data Team Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI), including Demos, deep-dives, and other recordings

### Data Team Handbook Navigation

- [Data Team Organization](/handbook/business-technology/data-team/organization)
- [How We Work](/handbook/business-technology/data-team/how-we-work)
    - [Calendar](/handbook/business-technology/data-team/how-we-work/calendar)
    - [Team Duties](/handbook/business-technology/data-team/how-we-work/duties)
    - [Data Analytics Team](/handbook/business-technology/data-team/organization/analytics/)
    - [Data Engineering Team](/handbook/business-technology/data-team/organization/engineering)
- [Data Platform](/handbook/business-technology/data-team/platform)
    - [Sisense (Periscope)](/handbook/business-technology/data-team/platform/periscope)
    - [dbt Guide](/handbook/business-technology/data-team/platform/dbt-guide)
    - [Enterprise Data Warehouse](/handbook/business-technology/data-team/platform/edw)
    - [Data Infrastructure](/handbook/business-technology/data-team/platform/infrastructure)
    - [SQL Style Guide](/handbook/business-technology/data-team/platform/sql-style-guide)
    - [Python Guide](/handbook/business-technology/data-team/platform/python-guide)
    - [Permifrost](/handbook/business-technology/data-team/platform/permifrost)
    - [Snowplow](/handbook/business-technology/data-team/platform/snowplow)
    - [Data CI Jobs](/handbook/business-technology/data-team/platform/ci-jobs)
    - [Sisense Style Guide](/handbook/business-technology/data-team/platform/sisense-style-guide)
- Data Programs
    - [Data for Finance](/handbook/business-technology/data-team/programs/data-for-finance)
    - [Data for Product Managers](/handbook/business-technology/data-team/programs/data-for-product-managers)
    - [Data Champion](/handbook/business-technology/data-team/direction/data-champion/)
    - [Data Catalog](/handbook/business-technology/data-team/data-catalog)
- [Data Quality](/handbook/business-technology/data-team/data-quality)
    - [Trusted Data Framework](/handbook/business-technology/data-team/platform/dbt-guide/#trusted-data-framework)
- [Data Quality Process](/handbook/business-technology/data-team/data-quality-process)
- [Data Team Principles](/handbook/business-technology/data-team/principles)
- [Data Service](/handbook/business-technology/data-team/data-service)
- [Data Handbook Documentation](/handbook/business-technology/data-team/documentation)
- [Data Learning and Resources](/handbook/business-technology/data-team/learning-library)


