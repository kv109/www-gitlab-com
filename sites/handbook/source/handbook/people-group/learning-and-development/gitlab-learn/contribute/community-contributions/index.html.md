---
layout: handbook-page-toc
title: Contribute to the Learning Experience Platform (LXP)
decscription: "Contribution process for wider community members"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contributing to GitLab Learn

At GitLab, [everyone can contribute](/company/mission-and-vision/#mission). This section outlines the contributions process to the GitLab Learn LXP by members of our wider community.

### Why contribute?

GitLab community members should feel welcomed and encouraged to contribute to the GitLab Learn LXP. The ability to contribute to GitLab Learn democratizes learning and enables our team members, customers, and community members to contribute to the growth of learning opportunities at GitLab. Some benefits for contributors include:

1. Share the work you've created to teach others how to use GitLab
1. Curate existing information that has helped you or others learn to use GitLab
1. Share information you've created about working remotely
1. Establish yourself as a subject matter expert within our learning community
1. Share learning pathways and other instructional design materials you've created
1. Enable organizations to learn about GitLab using innovative learning methods

### Contribution Examples

Review these examples for when you might consider contributing to GitLab Learn:

1. You find and article about working remotely that taught you a new skill or perspective
1. You've contributed to a GitLab handbook page that helps users use GitLab for project management
1. You've recorded a demo showing how you or your team uses GitLab that you'd like to share

### How to Contribute

1. Curate your learning material. Review the examples above for inspiration on how to get started.
1. Review our guidelines for [handbook first learning](/handbook/people-group/learning-and-development/interactive-learning/) to develop an understanding of how we build content for GitLab Learn
1. Open an issue on the [lxp-contributions issue board](https://gitlab.com/gitlab-com/people-group/learning-development/lxp-contributions/-/boards) using the template `lxp_contribution`.
1. Fill out the issue template to the best of your ability. The more information and justifiacation you and provide on the issue, the better!
1. The L&D team will moderate this issue board and use the ideas and resources provided on this board to design learning material for GitLab Learn. Please watch for direct tags from the L&D team in order to collaborate on your contribution issue!

### Future of contributing to the LXP

The GitLab Learning and Development team is iterating on the process for external audiences, including customers and community members, to contribute to GitLab Learn. As we iterate and scale, this process will change. We're always open to feedback on this process. If you have feedback, please reach out to our team via email at `learning@gitlab.com`.



