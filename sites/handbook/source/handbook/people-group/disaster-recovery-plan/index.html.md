---
layout: handbook-page-toc
title: Disaster Recovery Plan
description: Learn more about People Specialist and People Experience Teams disaster recovery plan.
---

## Disaster Recovery Plan

When a natural disaster or weather event occurs in a location that is near the home of a team member, the People Specialist and People Experience Teams (known as the People Operations Team) will send a notification email to all relevant team members, using the [template](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/natural_disaster_notification.md) found in the People Group project.   

What is a natural disaster and when do these policies go into effect?  Generally speaking a natural disaster is an event which has the potential to pose a significant threat to human health and safety, property, critical infrastuctures and national security. These natural disasters may come without any warning or they could be driven by seasonal weather patterns.  Different countries around the world have various ways of declaring an "Natural Disaster".  For example the office of [Disaster Management in India](https://ndma.gov.in) is responsible for managing and declaring Natural Disasters.  In the United States of America the [Department of Homeland Security](https://www.dhs.gov/natural-disasters) is responsible for making these Disaster declarations and the federal response.

### Team member assistance

GitLab reimburses ground transport to all team members to get out of the location they are in, via a vendor of choice e.g.: Uber/ Taxify/ Lyft/ Via etc. to a safe location.  Team members should use expensify for all reimbursements. 

### Time Off From Work

Team members should focus on the safety of their families and themselves first and leverage GitLab's [time off policy](/handbook/paid-time-off/). Your manager may contact you on a relatively frequent basis while you are out, just to ensure you have everything you need during this time.

### Team member resources

We understand that these events can be extremely stressful for team members and their families. We encourage all team members to utilize our Modern Health services. Modern Health is a company-sponsored, confidential and free resource available to you and your dependants. Services include confidential counseling, financial information and resources, legal support and more. Please refer to our [Modern Health](/handbook/total-rewards/benefits/modern-health/) page in the handbook for more details.

If you have any questions about team member benefits coverage, please contact our total rewards team at total-rewards@gitlab.com

### People Operations Team procedures for Disaster Recovery Plan

1. We get notified of these incidents and disasters by team members affected as well as other team members at GitLab. We also have an automated bot that feeds the #disaster-recovery-plan slack channel, from the [Global Disaster Alerts and Coordination](https://gdacs.org/). We do not react the green or orange notifications, but do react and action any alerts that are highlighted as "red". 
1. Helpful tools for identifying a disaster
    - [Google Public Alerts](https://google.org/publicalerts/map)
1. This is a shared task amongst the overall People Operations Team, a member on this team will reach out in the timezone of the team members affected by a natural disaster/ etc. via email. 
1. Pull a report from BambooHR utilizing cities, states, countries or locality.
1. Compare this report to the team page, identify if anyone is missing.
1. Please review the template before sending and add country/ location specific links and information. You can use one of the templates available [here](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/tree/master/.gitlab/email_templates).
1. To protect team member's privacy, only one email will be sent to all affected team members' Bcc'd. Please also cc `peoplepartners @gitlab.com`, `peopleops @gitlab.com` and `people-exp @gitlab.com`.
1. Send a separate email to the affected team members managers using this [template](.gitlab/email_templates/disaster_recovery_manager_email.md), CC'ing `peoplepartners @gitlab.com`, `peopleops@ gitlab.com` and `people-exp @gitlab.com`.
1. A Slack message will also be posted on the location Slack channel and if unavailable in the #team-member-updates channel or direct message. 
1. Once the emails have been sent, post a message in the `#disaster-recovery-plan` Slack channel, listing the number of team members affected. This channel will include leaders of legal, security and the people team. The People Operations or People Experience team will update the channel on their course of action and whether all team members have been reached during this process.
1. Using the previous BambooHR report, create columns for `email sent` and `team member responded` to manage the different affected team members responses (via email or Slack).
1. If there is no response from the team member/s within 24 hours, the People Operations Specialist and People Experience teams will send an email to the team member/s personal email address. Due to the vast number of time zones we operate in, and for transparency, please always cc peopleops@ gitlab.com in all communications.
1. If there is still no response after 48 hours, a People Operations Specialist or People Experience Associate will reach out to next-of-kin (this info can also be found in BambooHR) via email, and if no response, followed by a phone call. The People Operations Specialist or People Experience Associate will inform the team member's manager and [aligned People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) on the current status. 
1. If there is no response after 72 hours, a People Operations Specialist or People Experience Associate will attempt to reach out via email and phone (as needed) to all emergency contacts listed in BambooHR.
