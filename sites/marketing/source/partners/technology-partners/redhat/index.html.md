---
layout: solutions
title: GitLab on Red Hat
description: "Reduce time spent coding while still increasing productivity with technologies from GitLab and Red Hat."
canonical_path: "/partners/technology-partners/redhat/"
suppress_header: true
---

***
{:.header-start}

![Red Hat](/images/logos/Red_Hat_Logo_2019.svg)
{:.header-right}

# GitLab on Red Hat
{:.header-left}

Reduce time spent coding while still increasing productivity with technologies from GitLab and Red Hat.
{:.header-left}

[Talk to an expert](/sales/){: .btn .cta-btn .orange}
{:.header-left}

***
{:.header-end}

> With every handoff in IT, there is yet another opportunity for friction to slow the business and prevent innovation from reaching your customers. Because GitLab supports the entire delivery lifecycle and can deploy and manage your applications for you in Kubernetes, your teams can dramatically accelerate their delivery cadence and meet the expectations of the business. GitLab AutoDevOps can dramatically accelerate the time it takes to get a new application and a new pipeline up and running and, when combined with Red Hat® OpenShift®, can make development even smoother.

> Red Hat® OpenShift® Container Platform is a hybrid cloud application platform that runs across on-premises and public cloud infrastructures, enabling a hybrid approach to how applications can be deployed as a self-managed solution. It allows developers to deploy applications using a library of supported technologies that include Java, Node.js, .NET, Ruby, Python, PHP and more.

***

## Joint Red Hat and GitLab Benefits

Together, Red Hat OpenShift® Container Platform and GitLab enables enterprise and public sector customers to build and run scalable applications in modern dynamic environments, including public, private, and hybrid clouds.

GitLab has a certified operator for OpenShift that makes it easier to deploy and manage in a Kubernetes environment.

When running GitLab on Red Hat, software delivery cycle times are collapsed because the solution provides higher efficiency across all stages of the development lifecycle.


* Projects delivered on-time and budget
    * Eliminate bottlenecks for agility, faster DevOps lifecycle, reduce re-work, reduce unpredictable spend.
* Increase team productivity and velocity.
    * Attract, retain, and enable top talent, move engineers from integrations and maintenance to innovation, happy developers.
* Increase market share and revenue
    * Faster time to market, disrupt the competition; increased innovation; more expansive product roadmap.
* Increase customer satisfaction
    * Decrease security exposure, cleaner, and easier audits reduce disruptions.
{:.list-benefits}

[Learn more](https://www.openshift.com/try) about Red Hat Openshift and [how to install the GitLab operator](https://www.openshift.com/blog/installing-the-gitlab-runner-the-openshift-way)

***

## Red Hat platforms where GitLab is certified

* ![Red Hat OpenShift](/images/logos/Red_Hat-OpenShift.png) 
    * Red Hat provides the industry’s most comprehensive enterprise Kubernetes platform in Red Hat OpenShift. OpenShift is uniquely positioned to run containerized applications on public or private clouds. Through its GitLab operator, GitLab is certified with OpenShift to provide Day 1 and Day 2 operations.
* ![Red Hat Enterprise Linux](/images/logos/Red_Hat-Enterprise-Linux.svg)
    * Red Hat has a number of technologies in its portfolio. At the core is Red Hat Enterprise Linux®, the world’s leading enterprise-grade Linux operating system (OS) platform used by 100% of airlines, communications service providers, commercial banks, and healthcare companies in the Fortune Global 500.
    * Red Hat Enterprise Linux can be deployed across the hybrid cloud, from bare-metal and virtual servers to private and public cloud environments. Red Hat Enterprise Linux  makes it easier for operations teams to manage the upgrades, security patches and life cycles of servers being used to run applications like GitLab.
{:.list-capabilities}

***

## GitLab and Red Hat Joint Solutions in Action
{:.no-color}

* [GitLab and Red Hat: Automation to enhance secure software development](https://about.gitlab.com/blog/2020/04/29/gitlab-and-redhat-automation/)
* [Installing the GitLab Runner the OpenShift Way](https://www.openshift.com/blog/installing-the-gitlab-runner-the-openshift-way)
* [GitLab Runner on Red Hat Openshift](https://www.youtube.com/watch?v=5AbtSxpFQec&feature=youtu.be) 
{:.list-resources}

***
