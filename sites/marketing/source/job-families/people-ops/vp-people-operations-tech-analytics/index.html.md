---
layout: job_family_page
title: VP People Operations, Technology & Analytics
---

## Levels

### VP People Operations, Technology & Analytics

The VP, People Operations, Technology & Analytics reports to the [Chief People Officer](https://about.gitlab.com/job-families/people-ops/chief-people-officer/).

#### VP People Operations, Technology & Analytics Job Grade

The VP, People Operations, Technology & Analytics is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### VP People Operations, Technology & Analytics Responsibilities

* Optimize and maintain all people technology systems and tools. Identify opportunities to increase efficiency, automation and data flow.
Oversee development and management of thoughtful, scalable, People processes that balance our candidate and team member experience with compliance and legal requirements.
* Provide operational support and partner with G&A functions to execute on all people-related processes.
* Partner closely with peers and stakeholders across the People Group to assess existing People programs, processes, and practices, identify gaps and inefficiencies, and drive innovative solutions to enhance the team member experience.
* Translate legal and regulatory requirements into system or process requirements and monitor the effectiveness of control design.
* Oversee all People compliance processes and audits.
* Consult with legal teams and global People policy, process, business, and system owners to ensure sound protocols are in place.
* Own relevant process mapping and provide guidance to the business regarding technical solutions.
* Use people-related data to drive better decision making and develop key metrics to measure candidate and team member happiness, performance, engagement, and retention
* Conduct working sessions with users to gather, understand, and analyze business requirements.

#### VP People Operations, Technology & Analytics Requirements

* 7+ years of leading people operations in a high speed, high growth, technology-based environment
* Advanced project management experience in handling multiple projects at once with competing priorities
* 5+ years experience in managing people managers 
* Demonstrated experience supporting at least 2,000+ team members, across multiple countries ideally in a remote environment
* Experience with leading and implementation of large transformational initiatives
* Strong knowledge of People practices and procedures
* Proven track record in implementing and scaling People processes, operations and systems
* Experience building cross-functional partnerships and influencing stakeholders across the organization to act without having a direct reporting relationship
* Problem solver, able to troubleshoot issues independently or escalate when necessary; sense of accountability and sound professional judgment
* Proven analytical capabilities; experience with large amounts of data and in developing audit reports, metrics, and reporting mechanisms
* Experience building technology and people operations compliance programs and processes
* Organized, extremely detail oriented and execution focused
* Comfortable as a hands-on manager and willing to roll up your sleeves to get things done

## Performance Indicators
* [Onboarding Satisfaction Survey > 4.5](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
* [Onboarding task completion < X (TBD)](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd)
* [Bug to first action](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-bug-to-first-action)
* [Workscope done within a milestone](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-workscope-done-within-a-milestone)
* [Google drive documentation](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#google-drive-documentation)
* [Implementation of audits across Team Member Experience tasks](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#implementation-of-audits-across-team-member-experience-tasks)
* [Country Conversions completed within due date](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#country-conversions-completed-within-due-date) 
* [Increase the locations GitLab can offer work-sponsorship, in alignment with legal and tax](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#increase-the-locations-gitlab-can-offer-work-sponsorship-in-alignment-with-legal-and-tax)

## Career Ladder

The next step in the VP People Operations, Technology & Analytics job family is to move to the Chief People Officer job family.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates will have a 50 minute interview with the CPO
* Then, candidates will be invited to schedule threetwo separate 50 minute interviews; one with the Sr Director of People Success, and one with the Sr Manager of Total Rewards and the Director, People Operations
* Next, candidates will be invited to schedule 2 separate 50 minute interviews;  VP of IT and the Chief Financial Officer
* Finally, candidates may be invited to a 50 minute interview with the CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
