---
layout: markdown_page
title: "Product Career Development Framework Working Group"
canonical_path: "/company/team/structure/working-groups/product-career-development-framework/"
description: "Learn more about the Product Career Development Framework Working Group goals, processes, and teammates!"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | April 21, 2021 |
| Target End Date | September 30, 2021 |
| Slack           | [#wg_product-cdf](https://join.slack.com/share/zt-pg3dw504-aLrkdftFZ9xrqHEGqgqQMw) (only accessible by GitLab team members) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1a0baXkNsfDuDwcJ5IxNlLekCmFzElMGnmTmVAfQYV8o/edit?usp=sharing) (only accessible by GitLab team members) |
| Docs            | TBD |
| Epics/Issues    | [Main Epic]() / [Issue Board]() |
| Label           | `~wg-product-cdf` |
| Associated KPIs/OKRs | TBD |
| GitLab group for working group| `@wg-product-cdf |


## Business Goals

1. Deliver a revised [Product Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/#product-management-career-development-framework) for Individual Contributor roles to Group Manager of Product based on competencies by 2021-09-30. 
1. Conduct AMAs and training with Product Organization on new CDFs for use in FY22-Q4 Career Conversations. 

### Protocols and Processes

1. We will have a recurring weekly sync with the working group 
1. We will use a issue board for assigning tasks and delegating efforts across the working group 
1. We will track efforts in phases, which will be broken up into epics 
1. The DRI for the CDF is the VP of Product Management and all changes for the CDF will be approved via the VP

### Exit Criteria

1. Incorporation of research of Product Manager Skill that are industry standards 
1. Incorporation of research of internal expectations and clear rejection non-accepted internal expectations 
1. Quantative measures for progress included in the CDF 
1. Training for how to use the CDF for all managers 


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Anoop Dawar | VP, Product Management |
| Facilitator           | Jackie Porter | Group Manager, Product |
| Contributor           | Farnoosh Seifoddini | Principal PM, Product Operations|
| Functional Lead       |   |   |
| Functional Lead       |   |   |
| Functional Lead       |   |   |
| Functional Lead       |   |   |
| Functional Lead       |   |   |
| Functional Lead       |   |   |

## Working Groups

| Working Group    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Reviewer          | | |
| Reviewer          | | |
| Reviewer          | | |
| Reviewer          | | |

## Meetings

Meetings are recorded and available on
YouTube in the [Working Group - Product CDF]() playlist. The playlist is public, although, some meetings maybe private due to the nature of the material covered in the call. 

